package ru.geekbrains.java1.lesson1;
//Написать метод, принимающий на вход два числа, и проверяющий что их сумма лежит в
//пределах от 10 до 20(включительно), если да – вернуть true, в противном случае – false;

public class MainClass2 {
    public static void main(String[] args) {
        System.out.println(mySum(3, 7));
    }

    public static boolean mySum(int a, int b) {

        if (a + b >= 10 & a + b <= 20)
            return true;
        return false;
    }
}
