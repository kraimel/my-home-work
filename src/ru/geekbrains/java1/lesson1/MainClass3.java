package ru.geekbrains.java1.lesson1;

//Написать метод, которому в качестве параметра передается целое число, метод должен
//напечатать в консоль положительное ли число передали, или отрицательное; Замечание:
//ноль считаем положительным числом.

public class MainClass3 {
    public static void main(String[] args) {
        System.out.println(plusMinus(-1));
    }

    public static int plusMinus(int a) {
        if (a >= 0) {
            System.out.println("Положительное");
        } else {
            System.out.println("Отрицательное");
        }
        return a;
    }
}
