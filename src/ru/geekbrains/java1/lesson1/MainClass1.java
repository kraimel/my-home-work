package ru.geekbrains.java1.lesson1;

//Написать метод вычисляющий выражение a * (b + (c / d)) и возвращающий результат,
//где a, b, c, d – входные параметры этого метода;

public class MainClass1 {
    public static void main(String[] args) {
        System.out.println(myFormula(2,5,6,2));
    }

    public static int myFormula(int a, int b, int c, int d) {
        return a * (b + (c / d));
    }
}
