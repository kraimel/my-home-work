package ru.geekbrains.java1.lesson7;

public class Plate {
    private int food;

    public Plate(int food) {
        this.food = food;
    }
    public void info(){
        System.out.println("Plate " + food);
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }

    public void incFood(int food){ // вместо геттеров и сеттеров можно использовать методы
        this.food +=food;
    }

    public void decFood(int food){
        this.food -=food;
    }
}

