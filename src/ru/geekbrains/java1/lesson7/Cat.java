package ru.geekbrains.java1.lesson7;

public class Cat {
    private String name;
    private int appetite; // уровень прожорливости кота
    private boolean hungry;

    public Cat(String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
        this.hungry = true;
    }

    public void eat(Plate plt) { // взаимодействие  разных классов через метод
        //   plt.setFood(plt.getFood()-10);
        plt.decFood(appetite);
        hungry = false;
        System.out.println("name " + name + " - " + appetite + " кусков");
    }

    public void info() {
        System.out.println("name " + name + " - " + hungry);
    }

}
