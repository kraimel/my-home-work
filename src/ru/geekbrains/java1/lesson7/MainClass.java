package ru.geekbrains.java1.lesson7;

public class MainClass {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Barsic", 5);
        Cat cat2 = new Cat("Barsic1", 15);
        Cat cat3 = new Cat("Barsic2", 10);
        Plate plate = new Plate(100);
        //  cat.eat(plate); // вызов метода взаимодействия с конкретным объектом
        cat1.eat(plate);
        cat2.eat(plate);
        cat3.eat(plate);
        plate.info();
    }
}
