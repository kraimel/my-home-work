package ru.geekbrains.java1.lesson7homework;

public class MainClass {
    public static void main(String[] args) {
    Cat[] cats = new Cat[8];
        for (int i = 0; i < cats.length; i++) {
            cats[i] = new Cat("Barsic"+i,15+i*2);
        }
        Plate plate = new Plate(100);
        for (int i = 0; i < cats.length; i++) {
            cats[i].eat2(plate);
        }
          plate.info();
    }
}
