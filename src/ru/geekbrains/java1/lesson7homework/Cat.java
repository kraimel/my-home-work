package ru.geekbrains.java1.lesson7homework;

public class Cat {
    private String name;
    private int appetite;
    private boolean hungry;

    public Cat(String name, int appetite) {
        this.name = name;
        this.appetite = appetite;
        this.hungry = true; //параметр голода по умолчанию
    }

    public void eat(Plate plt) {
        if (plt.getFood() != 0) {
            if (!hungry) {
                System.out.println("Кот " + name + " не голоден");
            } else if (appetite <= plt.getFood()) {
                plt.setFood(plt.getFood() - appetite);
                hungry = false;
                System.out.println("Cat " + name + " eat " + appetite + " кусков");
            } else if (appetite > plt.getFood() & plt.getFood() != 0) {
                int ostatok = plt.getFood();
                plt.setFood(plt.getFood() - ostatok);
                System.out.println("Кот " + name + " хочет добавки");
                hungry = true;
            }
        } else {
            hungry = true;
            System.out.println("Кот " + name + " остался голодным");
        }
    }

    public void eat2(Plate plt) {
        if (hungry) {
            if (plt.getFood() != 0) {
                if (appetite <= plt.getFood()) {
                    plt.setFood(plt.getFood() - appetite);
                    System.out.println("Cat " + name + " eat " + appetite + " кусков");
                    hungry = false;
                } else if (appetite > plt.getFood()) {
                    int xochet = appetite - plt.getFood();
                    plt.setFood(0);
                    System.out.println("Кот " + name + " хочет добавки "+ xochet + " кусков");
                }

            } else {
                System.out.println("Кот " + name + " остался голодным");
            }
        } else {
            System.out.println("Кот " + name + " не голоден");
        }
    }

    public void info() {
        System.out.println("name " + name + " - " + hungry);
    }

}
