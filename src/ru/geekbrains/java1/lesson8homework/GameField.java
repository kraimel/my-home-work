package ru.geekbrains.java1.lesson8homework;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class GameField extends JPanel {
    private int SIZE = 3;
    private int DOTS_TO_WIN = 3;
    private int width; // длинна
    private int height; // высота
    private int cellWidth; // длинна ячейки
    private int cellHeight; //ширина ячейки
    private char[][] map;
    private Random rand;
    private boolean gameOver; //параметр для завершения игры
    private Font font; // создание шрифта
    private boolean noWiner;

    public GameField() {
        font = new Font("Times New Roman", Font.BOLD, 36); // установка значения шрифта
        map = new char[SIZE][SIZE];
        rand = new Random();

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (!gameOver) {
                    int clX, clY;
                    if (!isMapFull()) {
                        do {
                            clX = e.getX() / cellWidth;
                            clY = e.getY() / cellHeight;
                        } while (!isCellValid(clX, clY));
                        map[clX][clY] = 'X';
                        repaint();
                        if (checkWin('X')) {
                            gameOver = true;
                        }
                        aiTurn();
                    } else noWiner = true;
                }
            }
        });
        clear();
    }

    public void aiTurn() {
        int x = -1;
        int y = -1;
        if (!isMapFull()) {
            for (int i = 0; i < SIZE; i++) {
                for (int j = 0; j < SIZE; j++) {
                    if (isCellValid(j, i)) {
                        map[i][j] = 'X';
                        if (checkWin('X')) {
                            x = j;
                            y = i;
                        }
                        map[i][j] = '\u0000';
                    }
                }
            }
            if (x == -1 && y == -1) {
                do {
                    x = rand.nextInt(SIZE);
                    y = rand.nextInt(SIZE);
                } while (!isCellValid(x, y));
                map[x][y] = 'O';
                repaint();
                if (checkWin('O')) {
                    gameOver = true;
                }
            }
        } else noWiner = true;
    }

    public boolean isCellValid(int x, int y) {
        if (map[x][y] == '\u0000') return true;
        return false;
    }

    public boolean isMapFull() {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (map[i][j] == '\u0000') return false;
            }
        }
        return true;
    }

    public void clear() { // зачистка и отрисовки нового поля
        map = new char[SIZE][SIZE];
        gameOver = false;
        noWiner = false;
        repaint();
    }

    public boolean checkWin(char symb) {
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (checkLine(i, j, 1, 0, symb)) return true;
                if (checkLine(i, j, 0, 1, symb)) return true;
                if (checkLine(i, j, 1, 1, symb)) return true;
                if (checkLine(i, j, 1, -1, symb)) return true;
            }
        }
        return false;
    }

    public boolean checkLine(int x0, int y0, int vx, int vy, char symb) {
        if ((x0 + DOTS_TO_WIN * vx > SIZE) || (y0 + DOTS_TO_WIN * vy > SIZE) || (y0 + DOTS_TO_WIN * vy < -1))
            return false;
        for (int i = 0; i < DOTS_TO_WIN; i++) {
            if (map[y0 + i * vy][x0 + i * vx] != symb) return false;
        }
        return true;
    }

    @Override
    protected void paintComponent(Graphics g) { // метод отрисовки поля
        width = getWidth(); // узнаем высоту и длинну, присваиваем им значения
        height = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);

        g.setColor(Color.black);


        cellWidth = width / SIZE; // расчет размера ячеек
        cellHeight = height / SIZE;
        for (int i = 0; i <= SIZE; i++) {
            g.drawLine(0, i * cellHeight, width, i * cellHeight); // отрисовка по вертикали
            g.drawLine(i * cellWidth, 0, i * cellWidth, height); // отрисовка по горизонтали
        }
        g.drawRect(0, 0, width - 1, height - 1); // отрисовка границ поля
        for (int i = 0; i < SIZE; i++) { // после отрисовки клетки проходим по всей поверхности и проверяем введенные данные
            for (int j = 0; j < SIZE; j++) {
                if (map[i][j] == 'X') {
                    g.setColor(Color.GRAY);
                    g.fillOval(i * cellWidth + 5, j * cellHeight + 5, cellWidth - 10, cellHeight - 10); // отрисовка  овала (i * cellWidth + 5, j * cellHeight + 5, начальные координаты отрисовки)
                    // cellWidth - 10, cellHeight - 10 координаты длинны овала  взяты для тогог, чтоб овал влез в ячейку 100*100
                }
                if (map[i][j] == 'O') {
                    g.setColor(Color.BLACK);
                    g.fillOval(i * cellWidth + 5, j * cellHeight + 5, cellWidth - 10, cellHeight - 10); // отрисовка  овала (i * cellWidth + 5, j * cellHeight + 5, начальные координаты отрисовки)
                    // cellWidth - 10, cellHeight - 10 координаты длинны овала  взяты для тогог, чтоб овал влез в ячейку 100*100
                }
            }
            if (gameOver) { // если игра окончена, пишем шрифтом
                g.setFont(font);
                g.drawString("GAME OVER", 60, height / 2);
                g.setColor(Color.RED);
                g.drawString("GAME OVER", 62, height / 2 + 1);
            }
            if (noWiner) {
                g.setFont(font);
                g.drawString("NO ONE WIN", 60, height / 2);
                g.setColor(Color.RED);
                g.drawString("NO ORE WIN", 62, height / 2 + 1);
            }
        }

    }
}
