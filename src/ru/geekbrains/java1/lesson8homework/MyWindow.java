package ru.geekbrains.java1.lesson8homework;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindow extends JFrame {

    public MyWindow() {
        setTitle("My Window");
        setSize(346, 398);
        setLocationRelativeTo(null); // если так написать, окно появляется в центре экрана
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // данная операция завершает работу программы  после закрытия окна

        setResizable(false);


        JPanel jpBottom = new JPanel(); // Добавление панели
        add(jpBottom, BorderLayout.SOUTH); // указание где будет располагаться панель (сама панель по цвету такая же как и окно)
        jpBottom.setLayout(new GridLayout()); //  указание лояльности в панели
        JButton jbStart = new JButton("Start New Game");
        JButton jbExit = new JButton("Exit Game");
        jpBottom.add(jbStart); //Добавление  кнопок в панель
        jpBottom.add(jbExit);
        jpBottom.setPreferredSize(new Dimension(1, 40)); // для увеличения кнопок можно увеличить размер понели (причем по x не важно значение так как панель растягивается по длинне окна)


        jbExit.addActionListener(new ActionListener() { // добавление экшена на кнопку выхода с завершениеп работы программы без ошибки
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        GameField gf = new GameField();
        add(gf, BorderLayout.CENTER); // созданное поле прикрепляем в центр

        jbStart.addActionListener(new ActionListener() { // добавление действия на кнопку старт
            @Override
            public void actionPerformed(ActionEvent e) {
                gf.clear();
            }
        });
        setVisible(true); // указывает, что окно долджно быть видимвм, так как по умолчанию оно создается не видимым
    }
}
