package ru.geekbrains.java1.lesson6.animals;

public class Cat extends Animal {
    public Cat(String name, String color, float weight) {
        this.name = name;
        this.color = color;
        this.weight = weight;
    }

    @Override
    public void voice() {
        System.out.println(name + ": meow");
    }
    public void catMethod(){
        System.out.println(111);
    }
}
