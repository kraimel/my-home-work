package ru.geekbrains.java1.lesson6.animals;

public class Dog extends Animal { // extends  наследование все поля и методы родительского класса будут доступны классу, за исключением приватных
        // если в классе указать модификатор final это будет означать, что от него больше нет возможность наследоваться
    public Dog(String name, String color, float weight) {
        this.name = name;
        this.color = color;
        this.weight = weight;
    }

    @Override // аннотация для разрабюотчика указывающая на то, что метод был переопределен
    public void voice() { // переопределение метода (родительского)
        System.out.println(name + ": gav-gav");
    }
}
