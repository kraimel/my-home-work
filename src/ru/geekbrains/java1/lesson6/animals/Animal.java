package ru.geekbrains.java1.lesson6.animals;

public abstract class Animal { //указание в классе abstract горорит, что класс абстрактный и объект данного класса нельзя будет создать

    protected String name;  // модиыикатор protected позволяет использовать поля для дочерних классов
    protected String color; // не имеет значения в каком пакете находится наслнедник
    protected float weight; // если установить модификатор доступа по умолчанию,
    // использовать такое поле смогут только дочерние классы находящиеся в одном пакете с родителем

    public void info() {
        System.out.println("name: " + name + " color: " + color + " weight: " + weight + "kg");
    }

    public void walk() {
        System.out.println(name + " walking");
    }

    public void jump() {
        System.out.println(name + " jumping");
    }
    public void voice() { // если методу установить модификатор final то этот метод нельзся будет переопределить
        System.out.println(name + " :voice");
    }

//    public abstract voice() { // если класс абстрактный, то можно создать абстрактный метод не имеющий тела
//    } //  но при данной реализации появляется условие, что у каждого дочернего класса должен быть свой метод voice(Обязательно!!)
}
