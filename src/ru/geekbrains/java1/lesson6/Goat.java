package ru.geekbrains.java1.lesson6;

import ru.geekbrains.java1.lesson6.animals.Animal;

public class Goat extends Animal {
    public void abc(){
        color = "Red";  // доступ к полям осуществляется благодаря наследованию
    }
 }
