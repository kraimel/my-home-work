package ru.geekbrains.java1.lesson6;

import ru.geekbrains.java1.lesson6.animals.Animal;
import ru.geekbrains.java1.lesson6.animals.Cat;
import ru.geekbrains.java1.lesson6.animals.Dog;

public class MainClass {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Barsik", "Wight", 4.0f);
        Dog dog1 = new Dog("Bobik", "Black", 10.0f);
        Animal cat2 = new Cat("Murzik", "Brown", 5.0f); //  при условии, что родительский метод абстрактный,
        Animal dog2 = new Dog("Sharik", "Grey", 9.0f);  // можно создавать дочерние классы обращаясь к родительскому

        Animal[] anomals = {new Cat("Cat1", "W", 4.0f), new Dog("Dog1", "R", 7.0f)};
        //при использовании абстрактных классов можно создать массив разных дочерних классов
        if (cat2 instanceof Cat) // instanceof - экземпляр
            ((Cat) cat2).catMethod(); // так вызывается метод класса если объект был создан на основе абстрактного класса родителя
        //такое написание называется Каст (уточнение того, что в cat2 точно находится кот)

        cat1.jump();
        cat2.jump();
        cat1.walk();
        cat2.walk();

        dog1.info();

        cat1.voice();
        dog1.voice();
        cat2.voice();
        dog2.voice();
    }
}
