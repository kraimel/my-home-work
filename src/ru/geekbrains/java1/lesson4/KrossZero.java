package ru.geekbrains.java1.lesson4;
// игра крестики/нолики

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class KrossZero {

    public static int SIZE = 5; //размер игрового поля
    public static int DOTS_TO_WIN = 3; //колличестао точек для выйгрыша

    public static final char DOT_EMPTY = '࿁'; //отобржение не заполненных полей
    public static final char DOT_X = 'X'; //final  означает костанту и меняться это значение ниже по коду не может
    public static final char DOT_O = 'O';

    public static char[][] map;
    public static Scanner sc = new Scanner(System.in);
    public static Random rand = new Random(); //добавление рандома для логики работы компьтера

    public static void main(String[] args) {
        menu();
        initMap();
        printMap();

        while (true) {
            humanTorn(); // ход игрока
            printMap();

            if (checkWin(DOT_X)) { // при вызове метода берется значение из DOT_X в самом методе провверки все проводятся относительно DOT_X
                System.out.println("Победа!");
                break;
            }
            if (isMapFull()) {
                System.out.println("Ничья");
                break;
            }

            aiTorn(); // ход компьютера
            printMap();

            if (checkWin(DOT_O)) { // при вызове метода берется значение из DOT_O в самом методе провверки все проводятся относительно DOT_O
                System.out.println("AI WIN");
                break;
            }
            if (isMapFull()) {
                System.out.println("Ничья");
                break;
            }
        }
        System.out.println("Game Over!");
    }

    public static void menu() {
        System.out.println("Выбирите вариант игры \n1. Поле 3*3 Победа 3\n2. Поле 5*5 Победа 4\n3. Поле 8*8 Победа 5");
        int x;
        do {
            if (sc.hasNextInt()) //узнаем что подается в сканнер, если не число, то в Х записываем -1 и снова просим ввод
                x = sc.nextInt();
            else {
                sc.nextLine();
                x = -1;
            }
        } while (x < 1 || x > 3);

        switch (x) {
            case 1:
                SIZE = 3;
                DOTS_TO_WIN = 3;
                break;

            case 2:
                SIZE = 5;
                DOTS_TO_WIN = 4;
                break;

            case 3:
                SIZE = 8;
                DOTS_TO_WIN = 5;
                break;
        }
    }

    public static boolean isMapFull() { //метод проверки заполненности игрового поля
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (map[i][j] == DOT_EMPTY) return false;
            }
        }
        return true;
    }

    public static void humanTorn() { // логика хода игрока
        int x, y;
        do {
            System.out.println("Введите координаты в формате X Y");
            x = sc.nextInt() - 1; // отнимаем 1 так как пользователь вводит 1.2.3, массив нумеруется с нуля 0.1.2
            y = sc.nextInt() - 1;
        } while (!isCellValid(x, y));
        map[y][x] = DOT_X; // присваивается значение
    }

    public static void aiTorn() { // логика хода компьюьера
        int x = -1, y = -1;
        for (int i = 0; i < SIZE; i++) { //  интелект для помехи игроку
            for (int j = 0; j < SIZE; j++) {//  ai просматиривает поле и ставит точку игрока
                if (isCellValid(j, i)) {    // если при этом игрок выигрывает ai запоминает эту клетку
                    map[i][j] = DOT_X;
                    if (checkWin(DOT_X)) {
                        x = j;
                        y = i;
                    }
                    map[i][j] = DOT_EMPTY; // после проверки ai устанавливает значение пустого поля
                }
            }
        }
        if (x == -1 && y == -1) {
            do {

                x = rand.nextInt(SIZE); // рандомно выбирается значение от 0 до 2
                y = rand.nextInt(SIZE);
            } while (!isCellValid(x, y));
        }
        System.out.println("Компьютер походил в точку" + (x + 1) + " " + (y + 1));
        map[y][x] = DOT_O; // присваивается значение
    }

    public static boolean isCellValid(int x, int y) { // метод проверки валидности ячейки
        if (x < 0 || x >= SIZE || y < 0 || y >= SIZE) return false; // проверка на граници массива
        if (map[y][x] == DOT_EMPTY) return true;
        return false;
    }

    public static void initMap() { // создание игрового поля и заполнение его значениями по умолчанию
        map = new char[SIZE][SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                map[i][j] = DOT_EMPTY;
            }
        }
    }

    public static void printMap() { // печать карты
        System.out.print(" ");
        for (int i = 1; i <= SIZE; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        for (int i = 0; i < SIZE; i++) {
            System.out.print((i + 1) + " "); // печать номера строки
            for (int j = 0; j < SIZE; j++) {
                System.out.print(map[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public static boolean checkWin0(char symb) { //логина проверки победы (простой вариант для поля 3*3)не используется
        if (map[0][0] == symb && map[0][1] == symb && map[0][2] == symb) return true;
        if (map[1][0] == symb && map[1][1] == symb && map[1][2] == symb) return true;
        if (map[2][0] == symb && map[2][1] == symb && map[2][2] == symb) return true;

        if (map[0][0] == symb && map[1][0] == symb && map[2][0] == symb) return true;
        if (map[0][1] == symb && map[1][1] == symb && map[2][1] == symb) return true;
        if (map[0][2] == symb && map[1][2] == symb && map[2][2] == symb) return true;

        if (map[0][0] == symb && map[1][1] == symb && map[2][2] == symb) return true;
        if (map[2][0] == symb && map[1][2] == symb && map[0][2] == symb) return true;
        return false;
    }

    public static boolean checkWin(char symb) { //логина проверки победы (универсальный способ для лубой размерности поля)
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (checkLine(i, j, 1, 0, symb)) return true; // проверка возможности построения линии
                if (checkLine(i, j, 0, 1, symb)) return true;
                if (checkLine(i, j, 1, 1, symb)) return true;
                if (checkLine(i, j, 1, -1, symb)) return true;
            }
        }
        return false;
    }

    // алгоритм проверки линии для победы
    public static boolean checkLine(int x0, int y0, int vx, int vy, char symb) {
        if ((x0 + DOTS_TO_WIN * vx > SIZE) || (y0 + DOTS_TO_WIN * vy > SIZE) || (y0 + DOTS_TO_WIN * vy < -1))
            return false; // проверка на выход за пределы с права с низу и с верху
        for (int i = 0; i < DOTS_TO_WIN; i++) {
            if (map[y0 + i * vy][x0 + i * vx] != symb) return false; // координаты начинаются с Y
        }
        return true;
    }
}

