package ru.geekbrains.java1.lesson7homework2;

public class Ball {
    private String color;
    private Dog dog;

    public Ball(String color) {
        this.color = color;

    }

    public String getColor() {
        return color;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }
}
