package ru.geekbrains.java1.lesson7homework2;

public class Dog {
    private String name;
    private Ball ball;
    private int size;

    public Dog(String name, int size) {
        this.name = name;
        this.size = size;
    }

    // можно пользоваться данным методом при  попытке забрать мяч
    public void takeBall(Ball ball) { // если собака играет с каким-то мячом, она ео оставляет и берет новый
        if (this.ball != null) {
            System.out.println(name + " оставил " + this.ball.getColor() + " мяч");
            this.ball.setDog(null);
        }
        this.ball = ball;  // собаке назначается ссылка на мяч
        ball.setDog(this); // мячу назначается ссылка на новую собаку
    }

    public void play(Ball ball) {
        if (ball.getDog() == this) { // проверка, является ли собака вызвовшая метод хозяином меча в данный момент
            System.out.println(name + " Уже играет с этим мячом");
        } else if (ball.getDog() == null) {
            takeBall(ball); //  метод присваивания мяча
            System.out.println(name + " стал играть с " + ball.getColor() + " мячом");
        } else if (ball.getDog() != null) { // мехонизм борьбы собак за мяч
            if (size >= ball.getDog().size + 5) {
                System.out.println(name + " отобрал " + ball.getColor() + " мяч у " + ball.getDog().name);
                ball.getDog().ball = null; // у мяча убирается ссылка на прошлую собаку
                takeBall(ball);
            } else {
                System.out.println(name + " не может отобрать " + ball.getColor() + " мяч у " + ball.getDog().name);
            }
        }

    }

    public void stop() {
        if (ball != null) {
            System.out.println(name + " перестал играть с " + ball.getColor() + " мячом");
            ball.setDog(null);
            ball = null;
        } else {
            System.out.println(name +" не играет");
        }
    }

}
