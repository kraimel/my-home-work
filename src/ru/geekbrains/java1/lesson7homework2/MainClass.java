package ru.geekbrains.java1.lesson7homework2;

public class MainClass {
    public static void main(String[] args) {
        Dog dog1 = new Dog("Бобик", 3);
        Dog dog2 = new Dog("Тузик", 9);
        Ball ball1 = new Ball("красный");
        Ball ball2 = new Ball("зеленый");

         dog1.play(ball1);
         dog2.play(ball1);
         dog1.play(ball1);
         dog2.stop();
         dog1.play(ball1);
    }

}
