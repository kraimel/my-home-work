package ru.geekbrains.java1.lesson8;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class GameField extends JPanel {
    private int SIZE = 3;
    private int width; // длинна
    private int height; // высота
    private int cellWidth; // длинна ячейки
    private int cellHeight; //ширина ячейки
    private char[][] map;
    private Random rand;
    private boolean gameOver; //параметр для завершения игры
    private Font font; // создание шрифта

    public GameField() {
        font = new Font("Times New Roman", Font.BOLD, 36); //  установка значения шрифта
        map = new char[SIZE][SIZE];
        rand = new Random();
        // setBackground(Color.white); // заливаем цветом панель (в данный момент  не требуется так как заливка происходит в оттирсоке поля)
        addMouseListener(new MouseAdapter() { //отслеживание мыши используется MouseAdapter так как он настраиваемый, если использовать MouseListener потребуется описывать событие для каждого действия мыши
            @Override
            public void mouseReleased(MouseEvent e) { // отслеживание момента когда кнопку мыши отпустили
                //  System.out.println(e.getX() + " " + e.getY()); // отображение координат на которых была отпущена мыш
                if (!gameOver) {
                    int clX = e.getX() / cellWidth; // расчет с какой ячейки отпустили кнопку мыши (деление без остатка)
                    int clY = e.getY() / cellHeight;
                    map[clX][clY] = 'X';
                    repaint(); // для успешной отрисовки после расчета требуется вызвать "перерисовку", если задать координаты, то отрисуется только кусок поля соответствующий координатам
                    aiTurn();
                }
            }
        });
        clear(); // при запуске вызываем очистку поля
    }

    public void aiTurn() { //метод для хода  AI
        int x = rand.nextInt(SIZE); //  рандомно ваыбираются числа и кликается в координату
        int y = rand.nextInt(SIZE);
        map[x][y] = 'O';
        repaint();
    }

    public void clear() { //  метод зачистки поля и отрисовки нового поля
        map = new char[SIZE][SIZE];
        gameOver = false;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) { // метод отрисовки поля
        width = getWidth(); // узнаем высоту и длинну, присваиваем им значения
        height = getHeight();
        g.setColor(Color.white);
        g.fillRect(0, 0, width, height);

        g.setColor(Color.black);


        cellWidth = width / SIZE; // расчет размера ячеек
        cellHeight = height / SIZE;
        for (int i = 0; i <= SIZE; i++) {
            g.drawLine(0, i * cellHeight, width, i * cellHeight); // отрисовка по вертикали
            g.drawLine(i * cellWidth, 0, i * cellWidth, height); // отрисовка по горизонтали
        }
        g.drawRect(0, 0, width - 1, height - 1); // отрисовка границ поля
        for (int i = 0; i < SIZE; i++) { // после отрисовки клетки проходим по всей поверхности и проверяем введенные данные
            for (int j = 0; j < SIZE; j++) {
                if (map[i][j] == 'X') {
                    g.setColor(Color.GRAY);
                    g.fillOval(i * cellWidth + 5, j * cellHeight + 5, cellWidth - 10, cellHeight - 10); // отрисовка  овала (i * cellWidth + 5, j * cellHeight + 5, начальные координаты отрисовки)
                    // cellWidth - 10, cellHeight - 10 координаты длинны овала  взяты для тогог, чтоб овал влез в ячейку 100*100
                }
                if (map[i][j] == 'O') {
                    g.setColor(Color.BLACK);
                    g.fillOval(i * cellWidth + 5, j * cellHeight + 5, cellWidth - 10, cellHeight - 10); // отрисовка  овала (i * cellWidth + 5, j * cellHeight + 5, начальные координаты отрисовки)
                    // cellWidth - 10, cellHeight - 10 координаты длинны овала  взяты для тогог, чтоб овал влез в ячейку 100*100
                }
            }
            if (gameOver) { // если игра окончена, пишем шрифтом
                g.setFont(font);
                g.drawString("GAME OVER", 60, height / 2);
                g.setColor(Color.RED);
                g.drawString("GAME OVER", 62, height / 2 + 1);
            }
        }

    }
}
