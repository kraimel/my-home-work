package ru.geekbrains.java1.lesson8;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindow extends JFrame {

    public MyWindow() {
        setTitle("My Window");
        setSize(346, 398);
        setLocationRelativeTo(null); // если так написать, окно появляется в центре экрана
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE); // данная операция завершает работу программы  после закрытия окна

        setResizable(false); //  запрет изменения размера окна
       // setMinimumSize(new Dimension(200, 200)); //  установка минимального размера окна
        //  setMaximumSize(new Dimension(400,400));


        JPanel jpBottom = new JPanel(); // Добавление панели
        add(jpBottom, BorderLayout.SOUTH); // указание где будет располагаться панель (сама панель по цвету такая же как и окно)
        jpBottom.setLayout(new GridLayout()); //  указание лояльности в панели
        JButton jbStart = new JButton("Start New Game");
        JButton jbExit = new JButton("Exit Game");
        jpBottom.add(jbStart); //Добавление  кнопок в панель
        jpBottom.add(jbExit);
        jpBottom.setPreferredSize(new Dimension(1, 40)); // для увеличения кнопок можно увеличить размер понели (причем по x не важно значение так как панель растягивается по длинне окна)


        jbExit.addActionListener(new ActionListener() { // добавление экшена на кнопку выхода с завершениеп работы программы без ошибки
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        GameField gf = new GameField();
        add(gf, BorderLayout.CENTER); // созданное поле прикрепляем в центр

        jbStart.addActionListener(new ActionListener() { // добавление действия на кнопку старт
            @Override
            public void actionPerformed(ActionEvent e) {
                gf.clear();
            }
        });

//        JButton jb = new JButton("123");
//        add(jb);
//        jb.setBounds(200, 200, 100, 40); // указываются координаты окна и его размер
//        jb.addActionListener(new ActionListener() { // добавление действия на кнопку
//            @Override
//            public void actionPerformed(ActionEvent e) { // требуемое действие для конпки должно быть в actionPerformed
//                System.out.println("JAVA");
//            }
//        });

        //      setResizable(false);//  при необходимости запрещаем менять размеры окна
//        setLayout(new FlowLayout()); //  переключение на требуемый вариант отображения, если не указать переход, по умолчанию используется BorderLayout
        //BorderLayout  отображение кнопок по сторонам света
//        JButton jbl = new JButton("Next 1"); // создание кнопки
//        JButton jb2 = new JButton("Next 2");
//        JButton jb3 = new JButton("Next 3");
//        JButton jb4 = new JButton("Next 4");
//        JButton jb5 = new JButton("Next 5");
//        add(jbl, BorderLayout.CENTER); //  метов add привязывает кнопку к окну (BorderLayout.CENTER  добавление расположения кнопки)
//        add(jb2, BorderLayout.NORTH);
//        add(jb3, BorderLayout.SOUTH);
//        add(jb4, BorderLayout.WEST);
//        add(jb5, BorderLayout.EAST);
//FlowLayout отображение кнопок по очереди
//        JButton jbl = new JButton("Next 1"); // создание кнопки
//        JButton jb2 = new JButton("Next 2");
//        JButton jb3 = new JButton("Next 3");
//        JButton jb4 = new JButton("Next 4");
//        JButton jb5 = new JButton("Next 5");
//        jbl.setPreferredSize(new Dimension(200, 60)); // задание размера кнопке по необходимости
//        add(jbl); //  метов add привязывает кнопку к окну (BorderLayout.CENTER  добавление расположения кнопки)
//        add(jb2);
//        add(jb3);
//        add(jb4);
//        add(jb5);

// Также существуют GridLayout (сетка 3*3),  GridBagLayout (возможность создания не ровной сетки с указанием в % сколько занимать пространства в окне. Можно указать, что элемент занимает несколько клеток)
        //SpringLayout (распологает элементы привязывая их одного к другому)
        // если не хочется работать с Layout можно указать null


        setVisible(true); // указывает, что окно долджно быть видимвм, так как по умолчанию оно создается не видимым
    }
}
