package ru.geekbrains.java1.lesson5.homework;

public class Person {
    private String fio;
    private String position;
    private String email;
    private long phone;
    private int salary;
    private int age;

    public Person(String fio, String position, String email, long phone, int salary, int age) {
        this.fio = fio;
        this.position = position;
        this.email = email;
        this.phone = phone;
        this.salary = salary;
        this.age = age;
    }

    public void info() {
        System.out.println("Fio: " + fio + " Position: " + position + " Email: " + email + " Phone: " + phone + " Salary: " + salary + " Age: " + age);
    }

    public int getAge() {
        return age;
    }

    public String getFio() {
        return fio;
    }

    public long getPhone() {
        return phone;
    }

}
