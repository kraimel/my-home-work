package ru.geekbrains.java1.lesson5.homework;

public class MainClass {
    public static void main(String[] args) {
        Person[] persons = new Person[5];
        persons[0] = new Person("Alecsey", "IT_Admin", "qqq@mail.com", 79854141142l, 400, 20);
        persons[1] = new Person("Sergey", "Room cleener", "qww@mail.com", 78501214412l, 50, 50);
        persons[2] = new Person("Vitaliy", "Boss", "rr@mail.com", 74581215542l, 2000, 20);
        persons[3] = new Person("Nadejda", "Secretary", "tt@mail.com", 79581218846l, 200, 70);
        persons[4] = new Person("Vika", "Admin", "uu@mail.com", 79844331615l, 300, 20);

        for (Person i : persons) { // forich цикл проходит по всем элементам массива и проверяет условие
            if (i.getAge() > 40) {
                i.info();
            }
        }
        for (Person i : persons) {
            if (i.getPhone() == 74581215542l) {
                System.out.println(i.getFio());
            }
        }
        for (Person i : persons) {
            if (i.getFio() == "Vika") {
                System.out.println(i.getPhone());
            }
        }
    }
}
