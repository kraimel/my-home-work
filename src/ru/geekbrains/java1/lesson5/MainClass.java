package ru.geekbrains.java1.lesson5;

public class MainClass {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Barsic", "White", 2); // создание объекта cat1 по классу Cat "чертежу"
        Cat cat2 = new Cat("Murzik", "Black", 5);

        Cat[] cats = new Cat[10]; //  создание массива объектов (котов)
        cats[0] = new Cat("Vaska", "Grey", 3); // присвоение значения для одного объекта из массива

        cats[0].info(); // вызов процедуры для объекта из массива

        System.out.println(cat1.getName());
        cat1.setName("BlackBarsik");

        cat1.info();
        cat2.info();
        Cat.abc = 30; // обращение к полю класса

    }
}
