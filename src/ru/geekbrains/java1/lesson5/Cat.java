package ru.geekbrains.java1.lesson5;

public class Cat {
    public static int abc = 20; // обращение к подобным поляи происходит по имени класса Cat.abc = 30;
    private String name;
    private String color;
    private int weight;

    public void eat() { // подобными мметодами вожно будет взаимодействовать с полем у которого доступ private
        weight += 10;
    }

    public String getName() {// стандартный Геттер для получения имени
        return name;
    }

    public void setName(String name) { //Стандартный Сеттер для установки  значения в переменную
        this.name = name;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        if (weight > 0) // в сеттер устанавливается условие для отсичения не желательных значений
            this.weight = weight;
        else System.out.println("Ошибочный вес");
    }

    public Cat() { // конструктор по умолчанию
        name = "Unknown";
        color = "Unknown";
        weight = 1;
    }

    public Cat(String name, String color, int weight) { //конструктор должен иметь имя такоеже как и у класса.Должен быть без void
        this.name = name;
        this.color = color;
        this.weight = weight;
        if (weight < 0) throw new RuntimeException(); // таким образом ограничивается ввод данных для конструктора
    }

    public void info() {
        System.out.println("name: " + name + " color: " + color + " weight: " + weight + "kg");
    }

    public void jump() {
        System.out.println("name: " + name + " jumped");
    }
}
