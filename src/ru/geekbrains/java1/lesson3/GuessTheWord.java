package ru.geekbrains.java1.lesson3;

import java.util.Random;
import java.util.Scanner;

//* Создать массив из слов
//String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot",
//"cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea",
//"peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
//При запуске программы компьютер загадывает слово, запрашивает ответ у пользователя,
//сравнивает его с загаданным словом и сообщает правильно ли ответил пользователь. Если
//слово не угадано, компьютер показывает буквы которые стоят на своих местах.
//apple – загаданное
//apricot - ответ игрока
//ap############# (15 символов, чтобы пользователь не мог узнать длину слова)
//Для сравнения двух слов посимвольно, можно пользоваться:
//String str = "apple";
//str.charAt(0); - метод, вернет char, который стоит в слове str на первой позиции
//Играем до тех пор, пока игрок не отгадает слово
//Используем только маленькие буквы


//substring
//добавить при каждой итерации открывающуюся букву в слове


public class GuessTheWord {

    public static void main(String[] args) {
        System.out.println("Guess the word");
        do {
            String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado", "broccoli", "carrot",
                    "cherry", "garlic", "grape", "melon", "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea",
                    "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};
            Random random = new Random();
            String randomWord = words[random.nextInt(words.length)];

            do {
                int openWord = 1;

                for (int i = 0; i <= 3; i++) {
                    Scanner sc = new Scanner(System.in);
                    String enterWord = sc.next();

                    if (randomWord.contentEquals(enterWord)) {
                        System.out.println("Вы угадали");
                        break;
                    } else if (enterWord != randomWord) {
                        System.out.println("Вы ошиблись");

                        for (int j = 0; j < randomWord.length(); j++) {
                            String a = randomWord.substring(0, openWord); // substring работает со строками отображая элемены в указанном диапазоне
                            System.out.println(a + "######################");
                            System.out.println("The word was :" + randomWord);
                            break;

                        }
                        openWord++;

                    }
                    if ((i == 3)||(randomWord.length()<4)) {
                        System.out.println("You loose");
                        return;
                    }
                }

            } while (true);

        } while (true);
    }
}
