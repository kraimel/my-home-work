package ru.geekbrains.java1.lesson3;
//  заполнение массива звездочками

public class PrintArrey {
    public static void main(String[] args) {
        char[][] cx = new char[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                cx[i][j] = '*';

            }

        }
        printAr(cx);
    }

    //печать массива
    public static void printAr(char[][] c) {
        System.out.println("0 1 2 3 4"); // нумерация колонок
        for (int i = 0; i < c.length; i++) {
            System.out.print((i + 1) + " "); // нумерация сторок
            for (int j = 0; j < c[i].length; j++) {
                System.out.print(c[i][j] + " ");
            }
            System.out.println();
        }
    }

    // печать любого двумерного массива
    public static void printAnneyArrey(int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
            }
            System.out.println();
        }
    }
}
