package ru.geekbrains.java1.lesson3;

public class PeregruzkaMetoda {
    public static void main(String[] args) {
        add(2, 2);
        add(2f, 2f);
        add(2, 2, 3);
    }

    public static int add(int a, int b) {
        return a + b;
    }

    public static float add(float a, float b) {
        return a + b;
    }

    public static int add(int a, int b, int c) {
        return a + b + c;
    }
}
