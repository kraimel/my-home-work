package ru.geekbrains.java1.lesson3;

import java.util.Arrays;

public class MassInversion {
    public static void main(String[] args) {
        // task1();
        // System.out.println(isNegative(5));
        //  int[] arr = {4,8,8};
        // System.out.println(sumArrey(arr));
    }

    // инверсия массива
    public static void task1() {
        int[] arr = {1, 0, 1, 1, 1, 0};
        System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length; i++) {
            arr[i] = 1 - arr[i];
        }
        System.out.println(Arrays.toString(arr));
    }

    //проверка true  false
    public static boolean isNegative(int x) {
        if (x < 0) return true;
        return false;
    }

    // суммирование элементов массива
    public static int sumArrey(int[] x) {
        int w = 0;
        for (int i = 0; i < x.length; i++) {
            w += x[i];
        }
        return w;
    }

}