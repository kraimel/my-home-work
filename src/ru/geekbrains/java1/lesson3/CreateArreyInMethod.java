package ru.geekbrains.java1.lesson3;

import java.util.Arrays;

// создание одномерного массива через метод и заполнение его значениями
public class CreateArreyInMethod {
    public static void main(String[] args) {
        int[] a = createArrey(10, 8);
        System.out.println(Arrays.toString(a));
    }

    public static int[] createArrey(int len, int initValue) {
        int[] temp = new int[len];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = initValue;
        }
        return temp;
    }
}
