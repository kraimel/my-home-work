package ru.geekbrains.java1.lesson3;
//Написать программу, которая загадывает случайное число от 0 до 9, и пользователю дается 3
//попытки угадать это число. При каждой попытке компьютер должен сообщить больше ли
//указанное пользователем число, чем загаданное, или меньше. После победы или проигрыша
//выводится запрос – «Повторить игру еще раз? 1 – да / 0 – нет»(1 – повторить, 0 – нет).

import java.util.Random;
import java.util.Scanner;

public class LitleGameV2 {
    public static void main(String[] args) {
        System.out.println("Угадайте число от 0 до 10 У вас есть 3 попытки");
        do {
            Random random = new Random();
            int x = random.nextInt(10);
            for (int i = 0; i <= 2; i++) {
                System.out.println("Введите число");
                Scanner sc = new Scanner(System.in);
                int y = sc.nextInt();
                if (y == x) {
                    System.out.println("Вы угадали!");
                    break;
                }
                if (y > x) {
                    System.out.println("Загаданное число меньше вашего");
                }
                if (y < x) {
                    System.out.println("Загаданное число больше вашего");
                }
                if (i == 2) {
                    System.out.println("Больше попыток нет. Загаданное число было: " + x);
                    break;
                }
            }
            System.out.println("Попробовать еще раз?    1 - Да  0 - Нет");
            do {
                Scanner sc = new Scanner(System.in);
                int y = sc.nextInt();
                if (y == 1) {
                    System.out.println("Новая игра!!!");
                    break;
                }
                if (y == 0) {
                    System.out.println("Игра окончена.");
                    return;
                }
                if (y != 1 || y != 0) {
                    System.out.println("Введено не корректное значение");
                }
            } while (true);

        }
        while (true);
    }

}
