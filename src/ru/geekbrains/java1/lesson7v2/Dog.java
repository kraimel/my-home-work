package ru.geekbrains.java1.lesson7v2;

public class Dog {
    private String name;
    private Ball innerBall; // объект может "запомнить" ссылку на другой объект и работать с другим объектом (реализовано в геттере   this.ball = ball; )

    public Dog(String name) {
        this.name = name;
    }

    public void play(Ball ball) {
        if (!ball.isBusy()) { // если мяс не занят, то собака берет мяч
            ball.setBusy(true);
            this.innerBall = ball; //  при игре с мячом в ссылку записывается значение конкретного меча
            System.out.println(name + " playing with ball");
        } else {
            if (this.innerBall == ball) System.out.println(name + " i already playing"); //  проверка на то, что собака уже играет с мячом
            else
                System.out.println("Ball is busy :(");
        }
    }

    public void stop() {
        if (innerBall != null) {
            innerBall.setBusy(false);
            innerBall = null; // после прекращения игры с мячом ссылка освобождается
            System.out.println(name + " stop playing with ball");
        } else {
            System.out.println(name + ": i don't playing with any balls");
        }
    }
}
