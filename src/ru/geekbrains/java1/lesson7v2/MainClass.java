package ru.geekbrains.java1.lesson7v2;

public class MainClass {
    public static void main(String[] args) {
//        for (int i = 0; i < 10; i++) {
//            System.out.println("A");
//            try {
//                Thread.sleep(300); //  задержка при работе трай кетч
//            }catch (InterruptedException e){
//                e.printStackTrace();
//            }
//
//        }
        Ball ball = new Ball();
        Dog dog1 = new Dog("D1");
        Dog dog2 = new Dog("D2");

        dog1.play(ball);
        dog2.stop();
        dog2.play(ball);
        dog1.stop();
        dog2.play(ball);
        dog1.play(ball);
    }
}
