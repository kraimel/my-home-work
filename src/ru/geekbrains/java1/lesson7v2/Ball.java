package ru.geekbrains.java1.lesson7v2;

public class Ball {
    private boolean busy;

    public boolean isBusy() {
        return busy;
    }

    public void setBusy(boolean busy) {
        this.busy = busy;
    }

    public Ball() {
        this.busy = false;
    }
}
