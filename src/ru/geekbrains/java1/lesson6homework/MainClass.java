package ru.geekbrains.java1.lesson6homework;


import java.util.Random;

public class MainClass {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Barsik", 200, 0, 2.0f);
        Dog dog1 = new Dog("Sharik", 500, 10, 0.5f);
        Random random = new Random();
        int street = 200 + random.nextInt(700);
        int river = random.nextInt(15);
        float fence = 0.4f + random.nextInt((int) 2.5f);
        System.out.println("Препятствия: " + "Улица " + street + "m" + "  Река " + river + "m" + "  Забор " + fence + "m");

        new IsACross(street, cat1.getRunDistance());
        new IsACross(street, dog1.getRunDistance());

        new IsACross(river, cat1.getSwimDistance());
        new IsACross(river, dog1.getSwimDistance());

        new IsACross(fence, cat1.getJumpHeight());
        new IsACross(fence, dog1.getJumpHeight());
    }
}