package ru.geekbrains.java1.lesson6homework;

public abstract class Animal {

    protected String name;
    protected int runDistance;
    protected int swimDistance;
    protected float jumpHeight;

    public Animal(String name, int runDistance, int swimDistance, float jumpHeight) {
        this.name = name;
        this.runDistance = runDistance;
        this.swimDistance = swimDistance;
        this.jumpHeight = jumpHeight;
    }

    public void info() {
        System.out.println("name: " + name + " swimDistance: " + swimDistance + " runDistance: " + runDistance + "m/s" + "  jumpHeight: " + jumpHeight);
    }

    public void swim() {
        System.out.println(name + " swiming");
    }

    public void run() {
        System.out.println(name + " runing");
    }

    public void jumpOver() {
        System.out.println(name + " jumping over an obstacle");
    }

    public String getName() {
        return name;
    }

    public int getSwimDistance() {
        return swimDistance;
    }

    public int getRunDistance() {
        return runDistance;
    }

    public float getJumpHeight() {
        return jumpHeight;
    }

}
