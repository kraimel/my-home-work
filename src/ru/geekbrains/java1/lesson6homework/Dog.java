package ru.geekbrains.java1.lesson6homework;


public class Dog extends Animal {
    public Dog(String name, int runDistance, int swimDistance, float jumpHeight) {
        super(name, runDistance, swimDistance, jumpHeight);
    }
}