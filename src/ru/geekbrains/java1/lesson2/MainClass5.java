package ru.geekbrains.java1.lesson2;

import java.util.Arrays;
import java.util.Random;

//** Задать одномерный массив и найти в нем минимальный и максимальный элементы (без
//помощи интернета);
public class MainClass5 {
    public static void main(String[] args) {
        int min = 999;
        int max = 0;
        int[] arr = new int[10];
        Random random = new Random();
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(50);

        }
        System.out.println(Arrays.toString(arr));
        for (int i = 0; i < arr.length; i++) {
            if (min >= arr[i]) {
                min = arr[i];
            }
            if (max <= arr[i]) {
                max = arr[i];
            }

        }
        System.out.println("Минимальное значение =" + min);
        System.out.println("Максимальное значение =" + max);
    }
}
