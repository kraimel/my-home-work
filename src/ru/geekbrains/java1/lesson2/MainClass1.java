package ru.geekbrains.java1.lesson2;
//Задать целочисленный массив, состоящий из элементов 0 и 1. Например: [ 1, 1, 0, 0, 1, 0, 1, 1,
//0, 0 ]. С помощью цикла и условия заменить 0 на 1, 1 на 0;
import java.util.Arrays;

public class MainClass1 {
    public static void main(String[] args) {
        int[] arr1 = {1, 0, 1, 0, 0, 0, 1, 0, 1, 0};
        int[] arr2 = new int[arr1.length];
        //  arr[0] = 11; // запись значения в первую ячейку массива
        System.out.println(Arrays.toString(arr1));

        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] == 1) {
                arr2[i] = 0;
            }
            {
                if (arr1[i] == 0) {
                    arr2[i] = 1;
                }
            }
        }
        System.out.println(Arrays.toString(arr2));
    }
}
