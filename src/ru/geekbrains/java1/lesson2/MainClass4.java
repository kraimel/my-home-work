package ru.geekbrains.java1.lesson2;

//Создать квадратный двумерный целочисленный массив (количество строк и столбцов
//одинаковое), и с помощью цикла(-ов) заполнить его диагональные элементы единицами;
public class MainClass4 {
    public static void main(String[] args) {
        int arr[][] = new int[5][5];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if ((i == j) || (i == arr.length - 1 - j)) {
                    arr[i][j] = 1;
                }
            }
        }
        //Метод распечатки массива
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
    }}

// возможное написание процедуры для печати массива
//    public static void print2Array(int[][] arr) {
//        for (int i = 0; i < arr.length; i++) {
//            for (int j = 0; j < arr.length; j++) {
//                System.out.println(arr[i][j]);  // с лева стоит координата y, м права x
//            }
//            System.out.println();
//        }
//    }
//}