package ru.geekbrains.java1.lesson2;

import java.util.Arrays;

//Задать массив [ 1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1 ] пройти по нему циклом, и числа меньшие 6
//умножить на 2;
public class MainClass3 {
    public static void main(String[] args) {
        int[] arr1 = {1, 5, 3, 2, 11, 4, 5, 2, 4, 8, 9, 1};
        int[] arr2 = new int[arr1.length];
        System.out.println(Arrays.toString(arr1));

        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] < 6) {
                arr2[i] = arr1[i] * 2;
            } else {
                arr2[i] = arr1[i];
            }
        }
        System.out.println(Arrays.toString(arr2));
    }
}
// можно было решить так
//if(arr2[i]<6) arr2[i]*=2;