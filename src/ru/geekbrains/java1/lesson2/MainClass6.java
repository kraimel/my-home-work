package ru.geekbrains.java1.lesson2;
//Написать метод, в который передается не пустой одномерный целочисленный массив,
//метод должен вернуть true если в массиве есть место, в котором сумма левой и правой части
//массива равны. Примеры: checkBalance([1, 1, 1, || 2, 1]) → true, checkBalance ([2, 1, 1, 2, 1]) →
//false, checkBalance ([10, || 10]) → true, граница показана символами ||, эти символы в массив не
//входят.

public class MainClass6 {
    public static void main(String[] args) {
        int[] arr = {1, 1, 1, 3};
        System.out.println(chekBalanse(arr));
    }

    public static boolean chekBalanse(int[] arr) {
        for (int i = 1; i <arr.length ; i++) {
int suml = 0, sumr=0;
            for (int j = 0; j <i ; j++) {
                suml += arr[j];                
            }
            for (int j = i; j <arr.length ; j++) {
                sumr += arr[j];
            }
            if (suml == sumr) return  true;
        }
        return false;
    }
}