package ru.geekbrains.java2.lesson3homework1.work1;

import java.util.*;

public class CheckMach {

    public static Map<String, Integer> CheckMach(String[] arg) {
        ArrayList<String> arList = new ArrayList<>(Arrays.asList(arg)); // преобразование подаваемого массива в коллекцию
        Map<String, Integer> hmWord = new HashMap<>();
//        Set<String> hmWordSet = new HashSet<>();
        int sum = 1;
        for (int i = 0; i < arg.length; i++) {
            String word = arg[i];
            if (!hmWord.containsKey(word)) {
                hmWord.put(word, 1);
            }
            else {
                hmWord.put(word,sum++);
            }
        }
        return hmWord;
    }
}


//        Map<String,Integer> aaa = new HashMap<>();
//        aaa.get("");
//        aaa.containsKey("");
//        aaa.put("",0);
//        aaa.put("", aaa.get("") + 1);
