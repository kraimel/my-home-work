package ru.geekbrains.java2.lesson3homework1.work1;

import java.util.Random;

public class FullArray {
    private static Random random = new Random();

    public static String[] FullArray(String[] arg1, String[] arg2) {

        for (int i = 0; i < arg2.length; i++) {
            arg2[i] = arg1[random.nextInt(arg1.length)];
        }
        return arg2;
    }
}
