package ru.geekbrains.java2.lesson3homework1;

import java.util.HashMap;
import java.util.Map;

public class PhoneBook {
    public static void main(String[] args) {
        Map<Long, String> phoneBook = new HashMap<>();

        phoneBook.put(79851212120l, "Black");
        phoneBook.put(79851212121l, "Grey");
        phoneBook.put(79851212122l, "Smith");
        phoneBook.put(79851212123l, "Black");
        phoneBook.put(79851212124l, "Momoa");
        phoneBook.put(79851212125l, "Black");
        phoneBook.put(79851212126l, "Atlanna");
        phoneBook.put(79851212127l, "Momoa");
        phoneBook.put(79851212128l, "Dafoe");
        phoneBook.put(79851212129l, "Wilson");

//        searchByWord("Black", phoneBook);
        searchByNumber(89851212129l, phoneBook);
    }

    public static void searchByWord(String str, Map<Long, String> map) { // обязательно писать какие значения входят в мапу Map<Long,String> map
        for (Map.Entry<Long, String> entry : map.entrySet()) { //задается единица измерения: задается объект
            entry.getKey();
            entry.getValue();
            if (entry.getValue().contains(str)) {
                System.out.println("FIO : " + entry.getValue() + "      number: " + entry.getKey());
            }
        }
    }

    public static void searchByNumber(Long num, Map<Long, String> map) {
        for (Map.Entry<Long, String> entry : map.entrySet()) {
//            entry.getKey();
//            entry.getValue();
            String number = String.valueOf(num);
            String checkNumber = String.valueOf(entry.getKey());
            if (number.substring(1, (number.length())).equals(checkNumber.substring(1, (checkNumber.length())))) {
                System.out.println("FIO : " + entry.getValue() + "      number: " + entry.getKey());
            }
        }
    }
}

