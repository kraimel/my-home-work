package ru.geekbrains.java2.lesson5homework_done;

public class MainClass {
    public static void main(String[] args) {
        final int SIZE = 10000000;
        final int THREADS_COUNT = 4; // колличество потоков
        final int PART_SIZE = SIZE / THREADS_COUNT; // деление SIZE по потокам
        float[] mas = new float[SIZE];

        for (int i = 0; i < SIZE; i++) {
            mas[i] = 1;
        }
        long time = System.currentTimeMillis();
        float[][] m = new float[THREADS_COUNT][PART_SIZE];
        Thread[] t = new Thread[THREADS_COUNT];
        for (int i = 0; i < THREADS_COUNT; i++) {
            System.arraycopy(mas, PART_SIZE * i, m[i], 0, PART_SIZE);
            final int u = i;
            t[i] = new Thread(() -> {
                int z = u * PART_SIZE; //+ // в ананимный внутренний класс передавать можно только константы, поэтому создается final int u = i (константа с требуемым значением)
                for (int j = 0; j < PART_SIZE; j++, z++) {
                    m[u][j] = (float) (m[u][j] * Math.sin(0.2f + z / 5.0f) * Math.cos(0.2f + z / 5.0f));
                }
            });
            t[i].start();
        }

        try {
            for (int i = 0; i < THREADS_COUNT; i++) {
                t[i].join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < THREADS_COUNT; i++) {
            System.arraycopy(m[i],0,mas,i*PART_SIZE,PART_SIZE);
        }
        System.out.println("time = "+ (System.currentTimeMillis()));
    }
}

