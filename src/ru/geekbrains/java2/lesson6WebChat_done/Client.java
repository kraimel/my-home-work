package ru.geekbrains.java2.lesson6WebChat_done;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        Socket sock = null;
        Scanner console = new Scanner(System.in);
        try {
            sock = new Socket("localhost", 8189);
            PrintWriter out = new PrintWriter(sock.getOutputStream()); // обработчик на исходящий поток
            Scanner in = new Scanner(sock.getInputStream()); //обработчик на входящий поток
            // отсылка сообщений
            Thread t1 = new Thread(() -> {
                while (true) {
                    String str = console.nextLine();
                    out.println(str);
                    out.flush();
                    if (str.equals("end"))break; // выход из цикла после написания end
                }
            });
            // чтение сообщений от клиента
            Thread t2 = new Thread(() -> {
                while (true){
                    String str = in.nextLine();
                    if (str.equals("end"))break;
                    System.out.println("server^^ "+ str);

                }
            });
            t1.start();
            t2.start();
            try {
                t1.join();
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                sock.close();
                System.out.println("Client closed");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
