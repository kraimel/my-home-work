package ru.geekbrains.java2.lesson6WebChat_done;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public static void main(String[] args) {
        ServerSocket serv = null;
        Socket sock = null;
        Scanner console = new Scanner(System.in);
        try {
            serv = new ServerSocket(8189);
            System.out.println("Server started");
            sock = serv.accept();
            PrintWriter out = new PrintWriter(sock.getOutputStream()); // обработчик на исходящий поток
            Scanner in = new Scanner(sock.getInputStream()); //обработчик на входящий поток
            System.out.println("Client connected");
            // отсылка сообщений
            Thread t1 = new Thread(() -> {
                while (true) {
                    String str = console.nextLine();
                    out.println(str);
                    out.flush();
                    if (str.equals("end"))break; // выход из цикла после написания end, изначально отсылается сообщение а после проверяется на ключевое слово
                }
            });
            // чтение сообщений от клиента
            Thread t2 = new Thread(() -> {
                while (true){
                    String str = in.nextLine();
                    if (str.equals("end"))break;
                    System.out.println("Client^^ "+ str);

                }
            });
            t1.start();
            t2.start();
            try {
                t1.join();
                t2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                serv.close();
                System.out.println("Server closed");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
