package ru.geekbrains.java2.lesson2homework_done;

public class MatrixException extends Exception{
    public MatrixException(String message) {
        super(message);
    }
}
