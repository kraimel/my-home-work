package ru.geekbrains.java2.lesson2homework_done;

public class MainClass {
    public static void main(String[] args) {
        String str1 = "1 3 1 2\n2 3 2 2\n5 6 7 1\n3 3 1 0";
        try {
            System.out.println(calculateMatrix(str1));
        } catch (MatrixException e) {
            System.out.println(e.getMessage());
        }
    }

    public static int calculateMatrix(String str) throws MatrixException {
        int res = 0;
        String[] s = str.split("\n"); // берем массив строк и сплитим значение
        if (s.length != 4) throw new MatrixException("Не верное колличество строк");

        for (int i = 0; i < s.length; i++) {
            String[] st = s[i].split(" "); // проходим по всему массиву  и сплитим по новой в новый массив
            if (st.length != 4) throw new MatrixException("Не верное колличество столбцов в строке " + (i + 1));
            for (int j = 0; j < st.length; j++) {
                try {
                    res += Integer.parseInt(st[j]); //  снова проходим по всему массиву и складываем каждый элемент
                } catch (NumberFormatException e) {
                    throw new MatrixException("Элемент матрици[" + (i + 1) + "," + (j + 1) + "] не является числом");
                }
            }
        }
        return res / 2;
    }
}
