package ru.geekbrains.java2.lesson4swing;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyWindowChat extends JFrame {
    JTextArea jta; // элементы вынесены как глобальные для возможности работать с ними во всех методах
    JPanel jpAuth;
    JPanel jpMsg;
    JTextField jtfMsg;

    public MyWindowChat() {
        setTitle("Chat Client");
        setBounds(600, 300, 400, 400);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        jpAuth = new JPanel(new GridLayout()); // вынесенные элементы
        jpMsg = new JPanel(new BorderLayout());
        jta = new JTextArea(); // текстовое поле
//        ===============================================================
        jta.setEnabled(false); // делаем поле не доступное для ввода значений
        jta.setLineWrap(true); // делаем поле не доступное для скрала в бок (написанное будет переноситься на другую строку)
        JScrollPane jsp = new JScrollPane(jta); // добавление тескстовому полю возможности скролиться

        add(jpAuth, BorderLayout.NORTH); // размещение элементов по окну
        add(jpMsg, BorderLayout.SOUTH);
        add(jsp, BorderLayout.CENTER);

        JTextField jtfLogin = new JTextField(); // текстовое поле для ввода логина
        JPasswordField jpsPass = new JPasswordField(); // поле для ввода пароля, все введенные значения будут маскироваться точкой
        JButton jbAuth = new JButton("Auth"); // добавление кнопки авторизации

        jbAuth.addActionListener(new ActionListener() { // добавление действия на нажатие кнопки jbAuth
            @Override
            public void actionPerformed(ActionEvent e) {
                auth("A", "B");
            }
        });

        jpAuth.add(jtfLogin); // добавление созданных элементов в панель jbAuth
        jpAuth.add(jpsPass);
        jpAuth.add(jbAuth);

        jtfMsg = new JTextField(); // поле вынесено
        JButton jbSendMsg = new JButton("Send msg");
        jpMsg.add(jtfMsg, BorderLayout.CENTER);
        jpMsg.add(jbSendMsg, BorderLayout.EAST);
        jpMsg.setVisible(false);

        jbSendMsg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sendMsg();
            }
        });
        jtfMsg.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
        sendMsg();
            }
        });
        setVisible(true);
    }

    public void auth(String login, String rassword) { // метод аворизации
        jpAuth.setVisible(false);
        jpMsg.setVisible(true);
    }

    public void sendMsg() {
        jta.append(jtfMsg.getText()); // перенос написанного сообщения из поля jtfMsg в поле jta
        jta.append("\n"); // append = добавление текста к полю
        jtfMsg.setText(""); // очистка поля
        jtfMsg.grabFocus(); // установка фокуса в поле jtfMsg
    }
}
