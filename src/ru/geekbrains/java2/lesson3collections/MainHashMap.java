package ru.geekbrains.java2.lesson3collections;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;


public class MainHashMap {
    public static void main(String[] args) {
        HashMap<String,String> hm = new HashMap<>(); // "Словарь" с ключем и значением
        hm.put("Russia", "Moscow"); // записаны ключи и их значения, если добавлять запись с одинаковым ключем, тогда будет перезатирание
        hm.put("England", "London"); // если у разных записе     бедет одинаковый HashCode в таком случае они запишутся в одну ячейку, это колизия (не хорошая ситуация), их нужно стараться избегать
        hm.put("France", "Paris");
        hm.put("Germany", "Berlin ");

        System.out.println(hm.get("Russia")); // по ключу легко найти значение

//        LinkedHashMap     // на базе HashMap но запоминает расстановку элементов (запоминает связи)
//        TreeMap           // хронит записи в порядке возростания ключей
    }
}
