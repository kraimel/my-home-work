package ru.geekbrains.java2.lesson3collections;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class MainHashSet {
    public static void main(String[] args) {
        HashSet<String> hs = new HashSet<>(); //HashSet  - множество, HashSet не гарантирует порядок элементов
        hs.add("JAVA1");
        hs.add("JAVA1");
        System.out.println(hs.size()); // если запросить размерность то выдаст 1

//        если важен порядок требуется использовать LinkedHashSet
        Set<String> hs2 = new LinkedHashSet<>();
        hs2.add("JAVA1");
        hs2.add("JAVA2");
        System.out.println(hs2);

//        TreeSet аналогично TreeMap, записи хранятся по возростанию
        Set<Integer> hs3 = new TreeSet<>();
        hs3.add(20);
        hs3.add(5);
        hs3.add(15);
        System.out.println(hs3); // выравнивает записи по возростанию
    }
}
