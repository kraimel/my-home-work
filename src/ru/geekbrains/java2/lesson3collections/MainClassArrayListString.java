package ru.geekbrains.java2.lesson3collections;
//ArrayList - список, коллекция

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MainClassArrayListString {
    public static void main(String[] args) {
        ArrayList<String> als = new ArrayList<>(); //ArrayList - список в данный момент список стрингов
        // при создании списка ArrayList в нем по умолчанию 10 ячеек, информация должна быть без "разрывов" если есть разрыв, тогда происходит "выброс ячейки без информации"
        // при заполнении ArrayList больше 10 ячеек, его длинна увеличивается в 1.5 раза
        ArrayList<String> als2 = new ArrayList<>(); // если записать в скобки new ArrayList<>(1000) при создании будет создан ArrayList указанного размера
        als.add("Java"); //добавление в список происходит в конец списка
        als.add("Home");
        als.add("World");
        als.add("World3");
        als2.add("World3");
        als2.add("World3");
        als2.add("World3");
        als.remove("World3"); //удаление  из списка, по объекту или по индексу
        als.addAll(0, als2); // добавление всего содержимого второй коллекции в первую

//        als.ensureCapacity(5000); // метод ArrayList увеличения размерности до указанного значения, если размерность листа была больше, то она не изменится

        System.out.println(als);
        System.out.println(als.get(1)); // обращение к определенной ячейке списка ()
        System.out.println(10 + (10 >> 1)); //  + половина // >> знак битового сдвига на 1 в право

        LinkedList<String> linkedList = new LinkedList<>(); // лист хранящий в себе связи между элементами, поиск по такому листу происходит быстрее чес в обячном ArrayList
        LinkedList<Integer> lli = new LinkedList<>((List) Arrays.asList(new int[]{1, 2, 3})); // создаем массив, заворачиваем его в List, а после в LinkedList (asList это интерфейс)



    }
}
