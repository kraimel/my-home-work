package ru.geekbrains.java2.lesson3collections;

public class Box implements Comparable {
    //    private  int size;
    private int w;
    private int h;
    private int l;

    public Box(int w, int h, int l) {
        this.w = w;
        this.h = h;
        this.l = l;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Box) {
            Box another = (Box) obj; // делаем каст
            if (this.w == another.w && this.h == another.h && this.l == another.l)
                return true; //  просматривает поданное и сравнивает с шиблонным, если совпадает, тогда происходит удаление
        }
        return false;
    }

    public int volume() {
        return w * h * l;
    }

    @Override
    public String toString() {
        return "Box [volume: " + volume() + "]";
    }


    @Override
    public int hashCode() { // hashCode переписан и можно поставить любой расчет для него, может выйти так что hashCode будет одинаковым у разных объектов
        return w + h + l;
    }

    @Override
    public int compareTo(Object o) { // метод определения по каким критериям бедет проводится упорядочевание (сравнение)
        if (o instanceof Box){
            if (volume()>((Box) o).volume()) return 1; // если объект больше чем подаваемый возвращаем 1
            if (volume()<((Box) o).volume()) return -1; // если меньше то -1
            return 0; // если объемы равны возвращаем 0
        }
        throw  new RuntimeException("Not a Box!");
    }
}
