package ru.geekbrains.java2.lesson3collections;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class MainClassBox {
    public static void main(String[] args) {
        ArrayList<Box> alb = new ArrayList<>();
        alb.add(new Box(1, 1, 1)); // добавление в коллекцию массива "коробок" размерностью 3
//        alb.remove(new Box(5)); // таким образом объект не удалить из коллекции, он  останется в ней. Для возможности удаления требуется переотпеделить метод equals в классе BOX
        alb.add(new Box(2, 2, 2));
        System.out.println(alb); // при вызове печати  вызвается метод toString, если метод не переопределить то печатается имя класса после @ и за ним хешь код в 16-тиричной системе исчеслегния

        Set<Box> ts = new TreeSet<>();
        ts.add(new Box(1, 1, 1));
        ts.add(new Box(1, 2, 1));
        ts.add(new Box(1, 3, 1));

        Iterator<Box> iter = ts.iterator(); // итератор, у любой коллекции можно запросить итератор. Применяется в коллекциях так как применение обычных цыклов вызовет Exception
        while (iter.hasNext()) {
            Box b = iter.next();
            if (b.volume() == 1) iter.remove();
        }
        System.out.println(ts);
    }
}
