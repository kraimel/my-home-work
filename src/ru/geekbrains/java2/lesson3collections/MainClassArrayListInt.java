package ru.geekbrains.java2.lesson3collections;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainClassArrayListInt {
    public static void main(String[] args) {
        ArrayList<Integer> ali = new ArrayList<>();
        for (int i = 0; i < 19; i++) {
            ali.add(10);
        }
        ali.remove(10); // если убирать по индексу, то убирется только одно значение

//        ali.remove(new Integer(10)); // по объекту будут удален первый объект который попадется, для удаления сех требуется цыкл while ()
    }
}
