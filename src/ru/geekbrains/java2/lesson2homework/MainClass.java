package ru.geekbrains.java2.lesson2homework;

import java.util.Arrays;

public class MainClass {
    public static void main(String[] args) {
        String str1 = "1 3 1 2\n2 3 2 2\n5 6 7 1\n3 3 1 0";
        String str2 = "13 1 2\n2 32 2\n5 6 71\n3 3 1 0";
        String str3 = "1 3 1 2\n2 3 2 2\n5 6 7 1\n3 3 1 o";


//       System.out.print(Arrays.deepToString(convertToArray(str3)));

        System.out.println(sumArray(convertToArray(str2)));

    }

    static String[][] convertToArray(String s) {
        String[] clearN = s.split("\n");
        int length = clearN.length;
        String[][] result = new String[length][length];

        for (int i = 0; i < length; i++) {
            String[] clearSpaces = clearN[i].split(" ");
            for (int j = 0; j < length; j++) {
                try { // отлов ошибки размерности массива
                    result[i][j] = clearSpaces[j];
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Array size will must be 4");
//                    e.printStackTrace();
                }
            }

        }

        return result;
    }

    static int sumArray(String[][] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                try {
                    if (arr[i][j]== null) {
                        throw new IndexOutOfBoundsException("Array size error!!");
                    }
                    sum += Integer.parseInt(arr[i][j]);
                } catch (NumberFormatException e) {
                    System.out.println("There should be only numbers in the array, and length mast be 4");
//                    e.printStackTrace();
                }
            }
        }
        return sum / 2;
    }
}
