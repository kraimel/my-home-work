package ru.geekbrains.java2.lesson1v1;

public class Robot implements Participant {
    private String name;
    private int maxRun;
    private int maxJump;
    private boolean onDistance;

    public Robot(String name, int maxRun, int maxJump) {
        this.name = name;
        this.maxRun = maxRun;
        this.maxJump = maxJump;
        this.onDistance = true;
    }

    public boolean isOnDistance() {
        return onDistance;
    }

    @Override
    public void run(int dist) {
        if (dist <= maxRun) {
            System.out.println("Robot" + " " + name + " успешно пробежал кросс");
        } else {
            System.out.println("Robot" + " " + name + " не смог пробежать кросс");
            getOutFromDistance();
        }
    }

    @Override
    public void jump(int height) {
        if (height <= maxJump) {
            System.out.println("Robot" + " " + name + " успешно перепрыгнул препятствие");
        } else {
            System.out.println("Robot" + " " + name + " не смог перепрыгнуть препятствие");
            getOutFromDistance();
        }
    }

    @Override
    public void swim(int dist) {
        if (dist == 0) {
            System.out.println("Robot" + " " + name + " А воды-то и нет...");
            return;
        }
        System.out.println("Robot" + " " + name + " не умеет плавать");
        getOutFromDistance();
    }

    public void getOutFromDistance() {
        onDistance = false;
        System.out.println("Robot" + " " + name + " сошло с дистанции");
    }

    @Override
    public void printWin() {
        System.out.println("Robot" + " " + name + " прошел полосу препятствий");
    }
}


