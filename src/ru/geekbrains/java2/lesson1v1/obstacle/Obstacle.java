package ru.geekbrains.java2.lesson1v1.obstacle;

import ru.geekbrains.java2.lesson1v1.Participant;
import ru.geekbrains.java2.lesson1v1.animals.Animal; //  ранее вместо Participant был класс Animal

public abstract class Obstacle {
    public abstract void doIt(Participant participant);//  для любого препятствия потребуется реализовать метод doIt
}
