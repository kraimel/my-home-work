package ru.geekbrains.java2.lesson1v1.obstacle;

import ru.geekbrains.java2.lesson1v1.Participant;
import ru.geekbrains.java2.lesson1v1.animals.Animal;

public class Wall extends Obstacle {
    private int height;

    public Wall(int height) {
        this.height = height;
    }

    @Override
    public void doIt(Participant participant) {
        participant.jump(height);
    }
}
