package ru.geekbrains.java2.lesson1v1.obstacle;

import ru.geekbrains.java2.lesson1v1.Participant;
import ru.geekbrains.java2.lesson1v1.animals.Animal;

public class Water extends Obstacle {
    private int distance;
    private int waterTemp;

    public Water(int distance) {
        this.distance = distance;
    }

    @Override
    public void doIt(Participant participant) {
        participant.swim(distance);
    }
}
