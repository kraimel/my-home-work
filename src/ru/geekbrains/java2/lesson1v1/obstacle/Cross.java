package ru.geekbrains.java2.lesson1v1.obstacle;

import ru.geekbrains.java2.lesson1v1.Participant;
import ru.geekbrains.java2.lesson1v1.animals.Animal;

public class Cross extends Obstacle {
    private int distance;

    public Cross(int distance) {
        this.distance = distance;
    }

    @Override
    public void doIt(Participant participant) {
        participant.run(distance);
    }
}
