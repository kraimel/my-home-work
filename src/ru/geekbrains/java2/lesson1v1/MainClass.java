package ru.geekbrains.java2.lesson1v1;

import ru.geekbrains.java2.lesson1v1.animals.Animal;
import ru.geekbrains.java2.lesson1v1.animals.Cat;
import ru.geekbrains.java2.lesson1v1.animals.Dog;
import ru.geekbrains.java2.lesson1v1.obstacle.Cross;
import ru.geekbrains.java2.lesson1v1.obstacle.Obstacle;
import ru.geekbrains.java2.lesson1v1.obstacle.Wall;
import ru.geekbrains.java2.lesson1v1.obstacle.Water;

public class MainClass {
    public static void main(String[] args) {
        Participant[] participants = {new Robot("Chappy", 20000, 300), new Cat("Barsik"), new Dog("Bobik")}; // создание массива животных
        Obstacle[] course = {new Cross(200), new Water(2), new Wall(20)}; // создание массива препятствий

        for (int i = 0; i < participants.length; i++) { // берем животных и пробегаем по полосе препятствий
            for (int j = 0; j < course.length; j++) {
                course[j].doIt(participants[i]); // животное проходит препятствие
                if (!participants[i].isOnDistance()) {
                    break;
                }
            }
        }
        System.out.println("-----------------------------------");
        for (int i = 0; i < participants.length; i++) {
            if (participants[i].isOnDistance()) {
                participants[i].printWin();
            }
        }
    }
}
