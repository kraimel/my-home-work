package ru.geekbrains.java2.lesson1v1;

public interface Participant {
    void run(int dist);

    void swim(int dist);

    void jump(int height);

    boolean isOnDistance();
    void printWin();
}
