package ru.geekbrains.java2.lesson1v1.animals;

import ru.geekbrains.java2.lesson1v1.Participant;

//  можно сказать, что это просто протокол общения между классами,
// гарантия того, что у классов будут реализованны определенные методы
// (хоят методы могут быть реализованы совершенно по разному
public class Animal implements Participant { // класс Animal включает в себя интерфейс Participant
    protected String name;
    protected String type;
    protected int maxRunDistance;
    protected int maxJumpHeight;
    protected int maxSwimDistance;
    protected boolean onDistance;

    public Animal(String name, String type, int maxRunDistance, int maxJumpHeight, int maxSwimDistance) {
        this.name = name;
        this.type = type;
        this.maxRunDistance = maxRunDistance;
        this.maxJumpHeight = maxJumpHeight;
        this.maxSwimDistance = maxSwimDistance;
        this.onDistance = true;
    }
@Override // требуется ставить аннотацию о перезаписи  метода, так как методы находятся в интерфейсе
    public boolean isOnDistance() {
        return onDistance;
    }
@Override
    public void run(int dist) {
        if (dist <= maxRunDistance) {
            System.out.println(type + " " + name + " успешно пробежал кросс");
        } else {
            System.out.println(type + " " + name + " не смог пробежать кросс");
            getOutFromDistance();
        }
    }
@Override
    public void jump(int height) {
        if (height <= maxJumpHeight) {
            System.out.println(type + " " + name + " успешно перепрыгнул препятствие");
        } else {
            System.out.println(type + " " + name + " не смог перепрыгнуть препятствие");
            getOutFromDistance();
        }
    }
@Override
    public void swim(int dist) {
        if (dist == 0) {
            System.out.println(type + " " + name + " А воды-то и нет...");
            return;
        }
        if (maxSwimDistance == 0) {
            System.out.println(type + " " + name + " не умеет плавать");
            getOutFromDistance();
            return; // ставится для выхода из метода, чтоб не было сообщений о том, что участник не смог проплыть
        }
        if (dist <= maxSwimDistance) {
            System.out.println(type + " " + name + " успешно проплыл");
        } else {
            System.out.println(type + " " + name + " не смог проплыь");
            getOutFromDistance();
        }
    }

    public void getOutFromDistance() {
        onDistance = false;
        System.out.println(type + " " + name + " сошло с дистанции");
    }
@Override
    public void printWin() {
        System.out.println(type + " " + name + " прошел полосу препятствий");
    }
}
