package ru.geekbrains.java2.lesson1v1.animals;

public class Cat extends Animal {
    public Cat(String name) { // убраны значения из конструктора и установлены как значения по умолчанию
        super(name, "Кот", 500, 200, 0);
    }
}
