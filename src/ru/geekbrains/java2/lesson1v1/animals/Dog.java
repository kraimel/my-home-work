package ru.geekbrains.java2.lesson1v1.animals;

public class Dog extends Animal {
    public Dog(String name) {
        super(name, "Пес", 1000, 50, 20);
    }
}
