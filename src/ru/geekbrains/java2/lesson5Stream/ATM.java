package ru.geekbrains.java2.lesson5Stream;

public class ATM {
    private int money;

    public int getMoney() {
        return money;
    }

    public ATM(int money) {
        this.money = money;
    }

    public synchronized void takeMoney(String name, int amount) {
        if (money >= amount) {
            try {
                Thread.sleep(3);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            money -= amount;
            System.out.println(name + " take " + amount);
        }else {
            System.out.println(name + " Noe enough money");
        }
    }
}
