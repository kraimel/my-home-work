package ru.geekbrains.java2.lesson5Stream;

public class MainVariable {
    static int x;

    public static void main(String[] args) {

      Thread t1 =  new Thread(() -> { // свернуто через Replace with the limbs
            for (int i = 0; i < 10; i++) {
                x++;
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 =  new Thread(() -> { // свернуто через Replace with the limbs
            for (int i = 0; i < 10; i++) {
                x++;
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
        t2.start();

        try { //  работку некоторых моментов с потоком обязательно требуется брать в  try catch
            t1.join(); // поток t1 и t2 запускаются в потоке main и main будет ждать полной отработки поптоков // после сработает принт
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(x); // выводится  ноль так как для отработки потока  нужно время так как и для запуска потока, до того как поток успеет подготовиться  успевает сработать печать
    }
}
