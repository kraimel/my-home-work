package ru.geekbrains.java2.lesson5Stream;

public class MainPrintTen { // вариант создания потока врям в классе
    // class implements Runnable
    // @Run {           // создается класс который implements Runnable и реализуется Run
    //...
    //..}
    public static void main(String[] args) {
        new Thread(new Runnable() { // создается ананимный класс
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + " => " + i);
                    try {
                        Thread.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            }
        }).start(); // обязательно нужно указать .start();
    }
}
