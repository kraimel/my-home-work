package ru.geekbrains.java2.lesson5Stream;

public class Counter {
   private int c;

   public int getValue() {return c;}
    public Counter(){
        c = 0;
    }
    public synchronized void inc(){ // метод синхронезирован, это значит что с методом может в одно время работать только один поток
        c++;
    }
    public synchronized void dec(){
        c--;
    }
    public synchronized void synchTest(){
        System.out.println("Start");
//        synchronized (10obg){.....};   // в качестве монитора можно использовать внутри метода синхронизацию
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("End");
    }
}
