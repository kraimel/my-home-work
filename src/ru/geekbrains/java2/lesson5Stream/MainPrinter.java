package ru.geekbrains.java2.lesson5Stream;

public class MainPrinter {
    public static void main(String[] args) {
        Printer printer = new Printer();

        new Thread(() -> {printer.print("Doc #1", 5);}).start();
        new Thread(() -> {printer.scan("Doc #2", 5);}).start();
        new Thread(() -> {printer.print("Doc #3", 5);}).start();
    }
}
