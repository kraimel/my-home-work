package ru.geekbrains.java2.lesson5Stream;

public class MainCounter {

    public static void main(String[] args) {
        final Counter counter1 = new Counter();
        final Counter counter2 = new Counter();

       new Thread(new Runnable() {
            @Override
            public void run() {
                counter1.synchTest(); //  если два обекта, то можно метод вызавать одновременно у разных объектов
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                counter2.synchTest();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                counter1.synchTest();
            }
        }).start();


//        Thread t1 = new Thread(() -> { // первый поток увелисивает счетчик
//            for (int i = 0; i < 10; i++) {
//                counter.inc();
//                try {
//                    Thread.sleep(5);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        Thread t2 = new Thread(() -> {// второй поток уменшает счетчик
//            for (int i = 0; i < 10; i++) {
//                counter.dec();
//                try {
//                    Thread.sleep(5);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        });
//
//        t1.start();
//        t2.start();
//
//        try {
//            t1.join();
//            t2.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("Counret = " +counter.getValue()); // получаемое значение не всегда бывает ноль из-за особенности работы потоков
    }
}
