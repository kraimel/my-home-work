package ru.geekbrains.java2.lesson5Stream;

public class MainATM {
    public static void main(String[] args) {
        ATM atm = new ATM(100);

        Thread t1 = new Thread(() -> {atm.takeMoney("Bob #1", 50);});
        Thread t2 = new Thread(() -> {atm.takeMoney("Bob #2", 50);});
        Thread t3 = new Thread(() -> {atm.takeMoney("Bob #3", 50);});
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(atm.getMoney());
    }
}
