package ru.geekbrains.java2.lesson5Stream;

public class MainStream {
    public static void main(String[] args) {
//        MyThread mt1 = new MyThread();
//        MyThread mt2 = new MyThread();

//        mt1.run(); //   если писать run то метод выполняет то, что написаро в run, а не вызывает новый поток
//        mt1.start();
//        mt2.start();
        MyRunnable mr1 = new MyRunnable();
        MyRunnable mr2 = new MyRunnable();
        Thread t1 = new Thread(mr1); // в поток подается объект который реализует интерыейс Runnable
        Thread t2 = new Thread(mr2); // в поток можно дать только тот объект который implements Runnable

//        new Thread(new MyRunnable ()).start(); // короткая запись

        t1.start();
        t2.start();
    }
}
