package ru.geekbrains.java2.lesson5Stream;

public class Printer {
    private Object lockPrint = new Object();
    private Object lockScan = new Object(); // эта реализация позволяет проводить разные операци с одним объектом, в данный момент можно одновременно и печатать и сканироватью

 public void print(String docName, int count){
     synchronized (lockPrint){
        System.out.println("Начало печати");
     try {
         Thread.sleep(100*count);
     } catch (InterruptedException e) {
         e.printStackTrace();
     }
     System.out.println("Печать закончена");
    }}

    public void scan (String docName, int count){
     synchronized (lockScan){
        System.out.println("Начало сканирования");
        try {
            Thread.sleep(100*count);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Сканирование завершено");
    }}
}

