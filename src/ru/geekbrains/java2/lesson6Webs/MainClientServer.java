package ru.geekbrains.java2.lesson6Webs;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class MainClientServer {
    public static void main(String[] args) {
//        ServerSocket serv = new ServerSocket(8189); // прорисывается какой порт "слушает сервер" (порт выбран случайно)
        ServerSocket serv = null; // класс прослушки порта и ожидания подключения
        Socket sock;  // для определения подключенного создаем сокет (соединения с кем-либо из клиентов)
        try {
            serv = new ServerSocket(8189);
            System.out.println("Сервер запущен, ожидаем подключения...");
            sock = serv.accept(); // блокирующая операция, с этой строки сдвинуться нет возможности до момента подключения в сервис
            System.out.println("Пользователь подключился");
            Scanner sc = new Scanner(sock.getInputStream()); // для сканнера требуется подать поток
            PrintWriter pw = new PrintWriter(sock.getOutputStream()); // подаем исходящий поток (PrintWriter будет писать в исходящий поток)
       while (true){
           String str = sc.nextLine();
           if (str.equals("end"))break;
           pw.print("echo: "+str); // если написано что угодно кроме end  отправляется в ответ строка с припиской echo
           pw.flush(); // очистка буфера после записи строки
       }
        } catch (IOException e) {
            System.out.println("Server initialization exception");
        } finally {
            try {
                serv.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
