package ru.geekbrains.java2.lesson6Webs;

import java.io.*;

public class MainWebs {
    public static void main(String[] args) {
        try {
//            //если Reader или Writer, то это работа с текствовыми данными
//            BufferedReader br = new BufferedReader(new FileReader("1.txt")); // для чтения файлов лучше использовать BufferedReader
//            // BufferedReader (позволяет читать текстовые файлы по строчно или по элементно)
//            FileWriter fw = new FileWriter("2.txt");
//            fw.write("dasfasdqwe");
//            fw.close();
//            //  если FileOutput...Stream значит работа с байтами
//            FileOutputStream out = new FileOutputStream("1.txt");
//            out.write(65); //A
//            out.write(66); //B
//            out.write(67); //C
//            out.close();
            BufferedReader bin = new BufferedReader(new FileReader("1.txt"));
            int x;
            long t = System.currentTimeMillis();
            while ((x = bin.read()) != -1){

            }
            System.out.println(System.currentTimeMillis()-t);
            bin.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
