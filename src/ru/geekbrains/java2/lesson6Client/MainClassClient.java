package ru.geekbrains.java2.lesson6Client;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class MainClassClient {
    public static void main(String[] args) {
//        Socket sock = new Socket("localhost", 8189);
        Socket sock = null;
        try {
            sock = new Socket("localhost", 8189);
            Scanner scUser = new Scanner(System.in);
            Scanner sc = new Scanner(sock.getInputStream());
            PrintWriter pw = new PrintWriter(sock.getOutputStream());
            while (true) {
                String str = scUser.nextLine();
                pw.println(str);
                pw.flush();
                str = sc.nextLine();
                System.out.println(str);
            }
        } catch (IOException e) {
            System.out.println("Server initialization exception");
        } finally {
            try {
                sock.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
