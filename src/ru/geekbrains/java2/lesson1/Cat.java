package ru.geekbrains.java2.lesson1;

public class Cat {
    private String name; // такой уровень доступа говорит, что это поле доступно толь из данног класса
    String color; // если нет модификатора доступа, доступ к полю есть у всего пакета (доступ по умолчанию)
    protected int age; // к полю имеют доступ классы из пакета и любые наследники класса
    public int weight; // доступ со всех мест
    public static int agression; //  если у поля установлено значение static то его значение привязывается не к объекту, а к классу

    public Cat(String name, int agression) {
        this.name = name;
        this.agression = agression;
    }

    public void info() {
        System.out.println(name + " " + agression);
    }
}

