package ru.geekbrains.java2.lesson1;

public interface Flyable { // так называемый маркерный интерфейс
    // в интерфейсе можно создавать абстрактные методы
    void fly();//  все методы созданные в интерфейсе  по умолчанию считаются public abstract

    int x = 10;  // любая созданная переменная в интерфейсе подразумивается как public static final
}
