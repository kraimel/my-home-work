package ru.geekbrains.java2.lesson1;

public class MainClass {
    int x;
// inner  и nested  классы
    class A { // внетренний класс, он имеет доступ ко всем полям "обрамляющего" (внешнего) класса
        public void z() {
            x = 20;
        }

    }

    static class B { // внетренний класс, он имеет доступ ко всем полям "обрамляющего" (внешнего) класса
        public void z() {// имеет доступ только к статическим полям и методам внешнего класса
        }

    }

    public static void main(String[] args) {

        A a = new MainClass().new A(); //  создание объекта внутреннего класса (изначально создается объект внешнего класса MainClass2, после внетреннего)
        B b = new B(); // если класс статический, его можно создавать без указания создания вншнего класса
        Cat cat1 = new Cat("Barsik", 2);
        Cat cat2 = new Cat("Murzik", 5);
        //cat1.arression  не верное обращение к статическим полям обращаются через сам класс Cat.agression

        cat1.info();
        cat2.info();
        Flyable f1 = new Bird(); // таким образом можно создавать интерфейсы
        Flyable f2 = new Pegass();
        // Flyable f3 = new Cat(); // отобразится ошибка так как у кота нет интерфейса Flyable
        f2.fly();
        ((Bird) f1).chirick(); // каст в котором утверждается, что f1 это причка, после чего у нее появляется возможност использоватия метода чирик

        Months currentMonth = Months.April; // работа с  енумами
        System.out.println(currentMonth.number);
    }

    static class Bird implements Flyable { //класс bird умеет использовать интерфейс (летает) (через запятую можно добавлять и другие интерфейсы)

        @Override
        public void fly() {

        }

        public void chirick() {
            System.out.println("Chirik-Chirik");
        }
    }

    static class Pegass implements Flyable {

        @Override
        public void fly() {

        }
    }
}

