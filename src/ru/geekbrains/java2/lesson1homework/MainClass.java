package ru.geekbrains.java2.lesson1homework;

import ru.geekbrains.java2.lesson1homework.obstacle.Cross;
import ru.geekbrains.java2.lesson1homework.obstacle.Obstacle;
import ru.geekbrains.java2.lesson1homework.obstacle.Wall;
import ru.geekbrains.java2.lesson1homework.obstacle.Water;
import ru.geekbrains.java2.lesson1homework.teammembers.Cat;
import ru.geekbrains.java2.lesson1homework.teammembers.Dog;
import ru.geekbrains.java2.lesson1homework.teammembers.Human;
import ru.geekbrains.java2.lesson1homework.teammembers.Team;

public class MainClass {
    public static void main(String[] args) {
        Team[] team1 = {new Cat("Barsic"), new Dog("Charlick"), new Human("Elvis")};
        Team[] team2 = {new Cat("Murzik"), new Dog("Bobick"), new Human("Donna")};
        Obstacle[] obstacle = {new Cross(400), new Water(200), new Wall(50)};

        Proyti proyti = new Proyti();
        Result result = new Result();

        proyti.Proyti(team1, obstacle);
        proyti.Proyti(team2, obstacle);

        result.Result(team1);
        result.Result(team2);
    }
}
