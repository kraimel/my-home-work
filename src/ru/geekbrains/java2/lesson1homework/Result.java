package ru.geekbrains.java2.lesson1homework;

import ru.geekbrains.java2.lesson1homework.teammembers.Team;

public class Result {
    public void Result (Team[] team){
        for (int i = 0; i < team.length; i++) {
            if (team[i].isOnDistance()) {
                team[i].printWin();
            }
        }
    }
}
