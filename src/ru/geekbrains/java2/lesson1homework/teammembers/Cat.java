package ru.geekbrains.java2.lesson1homework.teammembers;

public class Cat extends Team {
    public Cat(String name) { // убраны значения из конструктора и установлены как значения по умолчанию
        super(name, "Кот", 500, 200, 0);
    }
}
