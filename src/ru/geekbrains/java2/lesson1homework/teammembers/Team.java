package ru.geekbrains.java2.lesson1homework.teammembers;

public class Team {
    protected String name;
    protected String type;
    protected int maxRunDistance;
    protected int maxJumpHeight;
    protected int maxSwimDistance;
    protected boolean onDistance;

    public Team(String name, String type, int maxRunDistance, int maxJumpHeight, int maxSwimDistance) {
        this.name = name;
        this.type = type;
        this.maxRunDistance = maxRunDistance;
        this.maxJumpHeight = maxJumpHeight;
        this.maxSwimDistance = maxSwimDistance;
        this.onDistance = true;
    }

    public boolean isOnDistance() {
        return onDistance;
    }

    public void run(int dist) {
        if (dist <= maxRunDistance) {
            System.out.println(type + " " + name + " успешно пробежал кросс");
        } else {
            System.out.println(type + " " + name + " не смог пробежать кросс");
            getOutFromDistance();
        }
    }

    public void jump(int height) {
        if (height <= maxJumpHeight) {
            System.out.println(type + " " + name + " успешно перепрыгнул препятствие");
        } else {
            System.out.println(type + " " + name + " не смог перепрыгнуть препятствие");
            getOutFromDistance();
        }
    }

    public void swim(int dist) {
        if (dist == 0) {
            System.out.println(type + " " + name + " А воды-то и нет...");
            return;
        }
        if (maxSwimDistance == 0) {
            System.out.println(type + " " + name + " не умеет плавать");
            getOutFromDistance();
            return;
        }
        if (dist <= maxSwimDistance) {
            System.out.println(type + " " + name + " успешно проплыл");
        } else {
            System.out.println(type + " " + name + " не смог проплыь");
            getOutFromDistance();
        }
    }

    public void getOutFromDistance() {
        onDistance = false;
        System.out.println(type + " " + name + " сошло с дистанции");
    }

    public void printWin() {
        System.out.println(type + " " + name + " прошел полосу препятствий");
    }
}

