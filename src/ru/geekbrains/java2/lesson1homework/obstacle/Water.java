package ru.geekbrains.java2.lesson1homework.obstacle;

import ru.geekbrains.java2.lesson1homework.teammembers.Team;


public class Water extends Obstacle {
    private int distance;
    private int waterTemp;

    public Water(int distance) {
        this.distance = distance;
    }

    @Override
    public void doIt(Team team) {
        team.swim(distance);
    }
}
