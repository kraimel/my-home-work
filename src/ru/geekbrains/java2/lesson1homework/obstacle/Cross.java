package ru.geekbrains.java2.lesson1homework.obstacle;


import ru.geekbrains.java2.lesson1homework.teammembers.Team;

public class Cross extends Obstacle {
    private int distance;

    public Cross(int distance) {
        this.distance = distance;
    }

    @Override
    public void doIt(Team team) {
        team.run(distance);
    }
}
