package ru.geekbrains.java2.lesson1homework.obstacle;

import ru.geekbrains.java2.lesson1homework.teammembers.Team;


public abstract class Obstacle {
    public abstract void doIt(Team team);//  для любого препятствия потребуется реализовать метод doIt
}
