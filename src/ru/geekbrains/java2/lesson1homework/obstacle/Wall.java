package ru.geekbrains.java2.lesson1homework.obstacle;


import ru.geekbrains.java2.lesson1homework.teammembers.Team;

public class Wall extends Obstacle {
    private int height;

    public Wall(int height) {
        this.height = height;
    }

    @Override
    public void doIt(Team team) {
        team.jump(height);
    }
}
