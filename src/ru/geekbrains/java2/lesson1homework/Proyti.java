package ru.geekbrains.java2.lesson1homework;

import ru.geekbrains.java2.lesson1homework.obstacle.Obstacle;
import ru.geekbrains.java2.lesson1homework.teammembers.Team;

public class Proyti {

    public void Proyti(Team[] team, Obstacle[] obstacles) {
        for (int i = 0; i < team.length; i++) {
            for (int j = 0; j < obstacles.length; j++) {
                obstacles[j].doIt(team[i]);
                if (!team[i].isOnDistance()) {
                    break;
                }
            }
        }
    }
}
