package ru.geekbrains.java2.lesson6WebChatServer_a_lot;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class ClientHendler {
    private Socket socket;
    private DataInputStream in;
    private DataOutputStream out;

    public ClientHendler(Socket socket) {
        try {
            this.socket = socket;
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread(() -> {
            try {
                while (true) {
                    String str = in.readUTF(); // может возникнуть исключение // выбрана кодировка UTF для чата, кодировки должны
                    // совпадать, в противном случае отобразится не корректная информация
                    if (str.equals("end"))break;
                    System.out.println("Client^^: "+ str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }
}
