package ru.geekbrains.java2.lesson2exception;

import java.io.FileInputStream;
import java.io.IOException;

public class FinallyException {
    public static void main(String[] args) {
        FileInputStream in = null;
        try {
            in = new FileInputStream("1.txt");
            // ... тут может быть большой кусок кода
        } catch (java.lang.Exception e) {
            System.out.println("AE");
        } finally { //  при реализации таким образом блок кода finally отрабатывает всегда  в независимости от того, будет поймана ошибка или нет
            System.out.println("Final");
            try {
                in.close(); //  при использовании файла  отрабатывает finally и файл "отпускается" принудительно
            }catch (IOException e){
                e.printStackTrace();
            }
        }
//        System.out.println(q());
    }
//    public static int q(){
//        try {
//            return 1;
//        }finally {
//            return 2;
//        }
//    }
}
