package ru.geekbrains.java2.lesson2exception;

public class ThrowException {
    public static void main(String[] args) {
        System.out.println("B");
        System.out.println(sqrt(4));
//        throw new RuntimeException(); // искуственный вброс Exception в программу
//        System.out.println("E");
    }

    public static float sqrt(float x) { //вычисление деления на 2
        if (x <0) throw  new RuntimeException("Negative namber");
        return x / 2.0f;
    }
}
