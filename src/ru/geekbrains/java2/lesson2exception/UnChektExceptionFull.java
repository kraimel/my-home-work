package ru.geekbrains.java2.lesson2exception;

public class UnChektExceptionFull {
    public static void main(String[] args) {
        try {
            System.out.println(4 / 0);
        } catch (RuntimeException e) { // отлов исключения по родителю
            System.out.println("RE");
        }
//        catch (ArithmeticException e) {// требуется собл.дать дерево иерархии ошибок и нужно с низу вверх(от меньшей к боьщей)
//            System.out.println("AE"); // в противном случае компилятор не пропустит так как меньший вариант
//                                    //  не отработает (не достижимый код)
//        }
        System.out.println("END");
    }
}
