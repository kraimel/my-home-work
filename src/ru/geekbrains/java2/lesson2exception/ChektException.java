package ru.geekbrains.java2.lesson2exception;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class ChektException {
    public static void main(String[] args) {
        try {
            FileInputStream in = new FileInputStream("1txt"); // если нет блока отлова ошибки, подчеркнуто красным
            // с информацией о возможности появления ошибки, обязательно требуется установить блок отлова ошибки
        } catch (FileNotFoundException e) {

        }

    }
}

