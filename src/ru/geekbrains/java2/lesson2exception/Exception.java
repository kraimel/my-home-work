package ru.geekbrains.java2.lesson2exception;

public class Exception extends Throwable {
    public static void main(String[] args) {
        new House(new Cat(), new Cat(), new Cat(), new Cat()); // так создается объект с аргументом переменной длинны
        System.out.println("END");
    }
}
