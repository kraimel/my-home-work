package ru.geekbrains.java2.lesson2exception;

import java.io.FileInputStream;
import java.io.IOException;

public class ThrowsException {
    public static void main(String[] args) {
//        readFile(); // подчеркивается так как нужно использовать try  catch
    }

    public static void readFile() throws IOException { // throws "контракт" для того кто будет пользоваться данным методом,
        // обязательно использовал метод отлова исключений. Если требуется можно поставить через запятую несколько исключений
        FileInputStream in = new FileInputStream("1.txt");
    }
}
