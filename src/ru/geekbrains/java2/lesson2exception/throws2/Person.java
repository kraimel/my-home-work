package ru.geekbrains.java2.lesson2exception.throws2;

public class Person {
    public Person (int age) throws PersonDataException { // указываем, что при использовании данного класса может возникнуть такая ошибка
        if(age <0){
            throw new PersonDataException("AGE!"); // указываем, что при создании может появиться ошибка
        }
    }
}
