package ru.geekbrains.java2.lesson2exception.throws2;

public class PersonDataException extends java.lang.Exception { // создан класс для отработки ошибки
    public PersonDataException(String message) {
        super(message);
    }
}
