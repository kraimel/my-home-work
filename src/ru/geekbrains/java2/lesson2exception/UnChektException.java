package ru.geekbrains.java2.lesson2exception;

import java.util.Scanner;

public class UnChektException {
    public static void main(String[] args) {
        //  try  catch  finally throw throws  система отлова исключений
        System.out.println("Begin");
        int a = 0, b = 0, c = 0;
        Scanner sc = new Scanner(System.in);
        int z[] = new int[5];
        try {
            a = 5;
            //    b = sc.nextInt();
            b = 0;
            z[20] = 20;
            c = a / b;
            System.out.println("X"); // если в отлавливаемом блоке появляется ошибка, ее перехватывает catch и программа
            // продолжает работу после блока catch, при такой ситуации кусок кода ниже исключения но в отлавливаемом блоке не проходит
        } catch (ArithmeticException e) {
            e.printStackTrace();
            // c = 10; в обработчике ошибок можно править ситуацию, в данный момент с =0 и в блоке меняем его на 10
        } catch (ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
        }
        System.out.println(c);
        System.out.println("AND");
    }
}
