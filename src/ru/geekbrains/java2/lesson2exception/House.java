package ru.geekbrains.java2.lesson2exception;

public class House {
    public House(Cat... cats) {// (Cat[] cats) в данный момент подается массив котов но если записать (Cat... cats) это
        // будет значить что подан набор котов (... аргумент переменной длинны)
        System.out.println("House with " + cats.length + " cats");
    }
}
