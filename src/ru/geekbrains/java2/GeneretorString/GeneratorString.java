package ru.geekbrains.java2.GeneretorString;

import java.util.Random;

public class GeneratorString {
    final static private String stringDigits = "1234567890";
    public static void main(String[] args) {
        String test ="+79## ### ## ##";
        System.out.println(generate(test));

    }

    public static String generate (String str){
        char[] podborka = stringDigits.toCharArray();
        char[] stroka = str.toCharArray();
        Random random = new Random();
        for (int i = 0; i < stroka.length; i++) {
            if (stroka[i] == '#'){
                stroka[i] = podborka[(int) (Math.random() *stringDigits.length())];
            }
        }
        String izmenennayStroka = String.valueOf(stroka);//  приведение обработанного массива в строку
        return izmenennayStroka;
    }
}
